// swift-tools-version:4.0

import PackageDescription

let package = Package(
    name: "NetworkingCore",
    products: [
        .library(
            name: "NetworkingCore",
            targets: ["NetworkingCore"])
    ],
    dependencies: [
        .package(url: "git@gitlab.com:msanford1030/SoS.git", from: "1.2.0"),
    ],
    targets: [
        .target(
            name: "NetworkingCore",
            dependencies: ["SwiftOnSockets"])
    ]
)
