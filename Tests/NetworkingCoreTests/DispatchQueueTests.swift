//
//  TimestampTests.swift
//
//  Created by Michael Sanford on 9/22/16.
//  Copyright © 2017 flipside5. All rights reserved.
//

import Foundation
import XCTest
import NetworkingCore

class DispatchQueueTests: XCTestCase {
    
    func testTimer() {
        let expectation = self.expectation(description: "Swift Expectations")
        
        var x = 1
        _ = DispatchQueue.main.addOperation(1.0) {
            x += 1
            expectation.fulfill()
        }
        
        waitForExpectations(timeout: 1.1) { _ in
            XCTAssertTrue(x == 2)
        }
    }
    
    func testTimerWithCancel() {
        let expectation = self.expectation(description: "Swift Expectations")
        
        var x = 1
        let item = DispatchQueue.main.addOperation(1.0) {
            x += 1
            //expectation.fulfill()
        }
        
        _ = DispatchQueue.main.addOperation(0.5) {
            item.cancel()
            expectation.fulfill()
        }
        
        waitForExpectations(timeout: 1.1) { _ in
            XCTAssertTrue(x == 1)
        }
    }
    
    func testRepeatTimer() {
        let expectation = self.expectation(description: "Swift Expectations")
        
        var x = 1
        let workItem = DispatchQueue.main.addOperation(1.0, repeats: true) {
            x += 1
        }
        
        _ = DispatchQueue.main.addOperation(3.5) {
            workItem.cancel()
            expectation.fulfill()
        }
        
        waitForExpectations(timeout: 3.6) { _ in
            XCTAssertTrue(x == 4)
        }
    }
    
    func testRepeatTimerComplex() {
        let expectation = self.expectation(description: "Swift Expectations")
        
        var x = 1
        let workItem = DispatchQueue.main.addOperation(1.0, repeats: true) {
            x += 1
        }
        
        _ = DispatchQueue.main.addOperation(3.5) {
            workItem.cancel()
        }
        
        _ = DispatchQueue.main.addOperation(5.5) {
            expectation.fulfill()
        }
        
        waitForExpectations(timeout: 7.5) { _ in
            XCTAssertTrue(x == 4)
        }
    }
}
