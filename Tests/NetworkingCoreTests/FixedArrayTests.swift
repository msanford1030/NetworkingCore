//
//  FixedArrayTests.swift
//
//  Created by Michael Sanford on 9/22/16.
//  Copyright © 2017 flipside5. All rights reserved.
//

import Foundation
import NetworkingCore
import XCTest

class FixedArrayTests: XCTestCase {

    func testAddRemove() {
        guard let fixedArray = FixedUnsafeArray<Int>(capacity: 3) else { XCTFail("FixedUnsafeArray init failed"); return }
        _testAddRemove(fixedArray: fixedArray)
    }
    
    func testThreadSafeAddRemove() {
        guard let fixedArray = FixedSafeArray<Int>(capacity: 3) else { XCTFail("FixedSafeArray init failed"); return }
        _testAddRemove(fixedArray: fixedArray)
    }
    
    private func _testAddRemove<T: FixedArray>(fixedArray: T) where T.ItemType == Int {
        var array = fixedArray
        XCTAssertTrue([] == array.ordered)
        XCTAssertTrue(array.count == 0)
        XCTAssertTrue(array.isEmpty == true)
        XCTAssertTrue(array.isFull == false)
        XCTAssertTrue(array[0] == nil)
        XCTAssertTrue(array[1] == nil)
        XCTAssertTrue(array[2] == nil)

        guard let index1 = array.add(10) else { XCTFail("index missing"); return }
        XCTAssertTrue(array[0] == 10)
        XCTAssertTrue(array[1] == nil)
        XCTAssertTrue(array[2] == nil)

        guard let index2 = array.add(20) else { XCTFail("index missing"); return }
        XCTAssertTrue(array[0] == 10)
        XCTAssertTrue(array[1] == 20)
        XCTAssertTrue(array[2] == nil)

        guard let index3 = array.add(30) else { XCTFail("index missing"); return }

        XCTAssertTrue([10, 20, 30] == array.ordered)
        XCTAssertTrue(array.count == 3)
        XCTAssertTrue(array.isEmpty == false)
        XCTAssertTrue(array.isFull == true)
        XCTAssertTrue(array[0] == 10)
        XCTAssertTrue(array[1] == 20)
        XCTAssertTrue(array[2] == 30)

        guard let item2 = array.remove(at: index2) else { XCTFail("item missing"); return }
        XCTAssertTrue([10, 30] == array.ordered)
        XCTAssertTrue(array.count == 2)
        XCTAssertTrue(array.isEmpty == false)
        XCTAssertTrue(array.isFull == false)
        XCTAssertTrue(item2 == 20)
        guard array.remove(at: index2) == nil else { XCTFail("item should be missing"); return }
        XCTAssertTrue(array[0] == 10)
        XCTAssertTrue(array[1] == nil)
        XCTAssertTrue(array[2] == 30)
        
        guard let item1 = array.remove(at: index1) else { XCTFail("item missing"); return }
        XCTAssertTrue([30] == array.ordered)
        XCTAssertTrue(array.count == 1)
        XCTAssertTrue(array.isEmpty == false)
        XCTAssertTrue(array.isFull == false)
        XCTAssertTrue(item1 == 10)
        guard array.remove(at: index1) == nil else { XCTFail("item should be missing"); return }
        XCTAssertTrue(array[0] == nil)
        XCTAssertTrue(array[1] == nil)
        XCTAssertTrue(array[2] == 30)
//        XCTAssertTrue(array[3] == nil)

        guard let index4 = array.add(40) else { XCTFail("index missing"); return }
        XCTAssertTrue([40, 30] == array.ordered)
        XCTAssertTrue(array.count == 2)
        XCTAssertTrue(array.isEmpty == false)
        XCTAssertTrue(array.isFull == false)
        XCTAssertTrue(array[0] == 40)
        XCTAssertTrue(array[1] == nil)
        XCTAssertTrue(array[2] == 30)
//        XCTAssertTrue(array[3] == nil)
        
        guard let item3 = array.remove(at: index3) else { XCTFail("item missing"); return }
        XCTAssertTrue([40] == array.ordered)
        XCTAssertTrue(array.count == 1)
        XCTAssertTrue(array.isEmpty == false)
        XCTAssertTrue(array.isFull == false)
        XCTAssertTrue(item3 == 30)
        guard array.remove(at: index3) == nil else { XCTFail("item should be missing"); return }
        XCTAssertTrue(array[0] == 40)
        XCTAssertTrue(array[1] == nil)
        XCTAssertTrue(array[2] == nil)

        guard let item4 = array.remove(at: index4) else { XCTFail("item missing"); return }
        XCTAssertTrue([] == array.ordered)
        XCTAssertTrue(array.count == 0)
        XCTAssertTrue(array.isEmpty == true)
        XCTAssertTrue(array.isFull == false)
        XCTAssertTrue(item4 == 40)
        guard array.remove(at: index4) == nil else { XCTFail("item should be missing"); return }
        XCTAssertTrue(array[0] == nil)
        XCTAssertTrue(array[1] == nil)
        XCTAssertTrue(array[2] == nil)
    }
    
    
    @available(macOS 10.12, *)
    func testThreadness() {
        
        guard let fixedArray = FixedSafeArray<Int>(capacity: 16) else { XCTFail("FixedArray init failed"); return }
        
        var array = fixedArray
        var ready = false
        
        // writer
        Thread.detachNewThread {
            ready = true
            for step in 0...10000 {
                switch step % 2 {
                case 0:
                    let _ = array.add(23)
                case 1:
                    let _ = array.remove(at: 0)
                default:
                    XCTAssertTrue(false, "should never happend")
                }
            }
        }
        
        // reader
        Thread.detachNewThread {
            while !ready {}
            
            for _ in 0...10000 {
                if let value = array[0] {
                    XCTAssertTrue(value == 23, "value is horked")
                }
//                if (step % 250) == 0 {
//                    print("step=\(step)")
//                }
            }
        }
        
        // reader
        Thread.detachNewThread {
            while !ready {}

            for _ in 0...10000 {
                if let value = array[0] {
                    XCTAssertTrue(value == 23, "value is horked")
                }
            }
        }
    }
}
