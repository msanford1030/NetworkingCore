//
//  ByteCodingTests.swift
//  NetworkingCore
//
//  Created by Michael Sanford on 5/16/17.
//
//

import Foundation
import NetworkingCore
import XCTest

class ByteCodingTests: XCTestCase {
    
    func testIntCoding() {
        do {
            let archiver = ByteArchiver()
            let preValue: Int = 19
            archiver.encode(preValue)
            let unarchiver = ByteUnarchiver(archive: archiver.archive)
            let postValue: Int = try unarchiver.decodeInt()
            XCTAssertTrue(preValue == postValue)
        } catch {
            XCTAssert(false, "something went wrong when decoding. error=\(error)")
        }
    }
    
    private func encodeDecode(string: String, asAscii: Bool) {
        do {
            let archiver = ByteArchiver()
            archiver.encode(string, asAscii: asAscii)
            let unarchiver = ByteUnarchiver(archive: archiver.archive)
            let postValue = try unarchiver.decodeString(asAscii: asAscii)
            XCTAssertTrue(string == postValue)
        } catch {
            XCTAssert(false, "something went wrong when decoding. error=\(error)")
        }
    }

    func testAsciiStringCoding() {
        encodeDecode(string: "Michael Sanford", asAscii: true)
    }
    
    func testBigAsciiStringCoding() {
        encodeDecode(string: "Michael Sanford😀", asAscii: true)
    }
    
    func testNonAsciiStringCoding() {
        encodeDecode(string: "Michael Sanford", asAscii: false)
    }
    
    func testBigNonAsciiStringCoding() {
        encodeDecode(string: "Michael Sanford😀", asAscii: false)
    }
}
