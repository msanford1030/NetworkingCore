//
//  QueueTests.swift
//
//  Created by Michael Sanford on 4/16/17.
//  Copyright © 2017 flipside5. All rights reserved.
//

import XCTest
import NetworkingCore

class QueueTests: XCTestCase {
    
    func testEnqueueDequeue() {
        var queue = Queue<Int>()
        queue.add(1)
        queue.add(2)
        queue.add(3)
        
        XCTAssertTrue(queue.count == 3)
        
        XCTAssertTrue(queue.remove() == 1)
        XCTAssertTrue(queue.remove() == 2)
        XCTAssertTrue(queue.remove() == 3)
        
        XCTAssertTrue(queue.count == 0)
    }
    
    func testOrderOfItems() {
        var queue = Queue<Int>()
        queue.add(1)
        queue.add(2)
        queue.add(3)
        
        XCTAssertTrue(queue.ordered == [1,2,3])
        
        XCTAssert(queue.ordered.contains(1))
        XCTAssert(queue.ordered.contains(2))
        XCTAssert(queue.ordered.contains(3))
    }
    
    func testLotsOfEnqueueDequeues() {
        var queue = Queue<Int>()
        
        let innerStep = 10
        for _ in 0 ..< 100000 {
            for pushStep in 0 ..< innerStep {
                queue.add(pushStep)
            }
            XCTAssert(queue.count == innerStep)
            for _ in 0 ..< innerStep {
                _ = queue.remove()
            }
            XCTAssert(queue.count == 0 && queue.isEmpty)
        }
    }

    func testDequeue() {
        var queue = Queue<Int>()
        
        XCTAssertTrue(queue.isEmpty)
        XCTAssertTrue(queue.remove() == nil)
        XCTAssertTrue(queue.first == nil)
        
        queue.add(1)
        XCTAssertTrue(queue.first == 1)
        XCTAssertTrue(!queue.isEmpty)
        
        queue.add(2)
        XCTAssertTrue(queue.first == 1)
        XCTAssertTrue(!queue.isEmpty)
        
        queue.add(3)
        XCTAssertTrue(queue.first == 1)
        XCTAssertTrue(!queue.isEmpty)
        
        XCTAssertTrue(queue.remove() == 1)
        XCTAssertTrue(queue.first == 2)
        XCTAssertTrue(!queue.isEmpty)
        
        XCTAssertTrue(queue.remove() == 2)
        XCTAssertTrue(queue.first == 3)
        XCTAssertTrue(!queue.isEmpty)
        
        XCTAssertTrue(queue.remove() == 3)
        
        XCTAssertTrue(queue.isEmpty)
        XCTAssertTrue(queue.first == nil)
        XCTAssertTrue(queue.remove() == nil)
        XCTAssertTrue(queue.isEmpty)
    }
    
    func testInit() {
        var queue = Queue<Int>([1, 2, 3])
        XCTAssertTrue(queue.count == 3)
        XCTAssertTrue(queue.ordered == [1,2,3])
        XCTAssertTrue(queue.remove() == 1)
        XCTAssertTrue(queue.remove() == 2)
        XCTAssertTrue(queue.remove() == 3)
        XCTAssertTrue(queue.remove() == nil)
    }
    
    func testImmutables() {
        let queue = Queue<Int>([1, 2, 3])
        
        let (queue2, item) = queue.removed()
        XCTAssertTrue(queue.count == 3)
        XCTAssertTrue(queue2.count == 2)
        XCTAssertTrue(queue2.ordered == [2, 3])
        XCTAssertTrue(item == 1)
        
        let queue3 = queue2.added(4)
        XCTAssertTrue(queue2.count == 2)
        XCTAssertTrue(queue3.count == 3)
        XCTAssertTrue(queue3.ordered == [2, 3, 4])
        
        let queue4 = Queue<Int>()
        XCTAssertTrue(queue4.count == 0)
        let (queue5, item5) = queue4.removed()
        XCTAssertTrue(queue5.count == 0)
        XCTAssertTrue(item5 == nil)
    }
}
