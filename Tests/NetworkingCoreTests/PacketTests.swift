//
//  PacketTests.swift
//
//  Created by Michael Sanford on 10/14/16.
//  Copyright © 2017 flipside5. All rights reserved.
//

import Foundation
import XCTest
import NetworkingCore

class PacketTests: XCTestCase {
    let session = RTPSession(ssrc: 123)
        
    func testSequenceNumber() {
        var sequenceNumber: RTPSequenceNumber = UInt16.max - UInt16(32)
        for _ in 0 ..< 100 {
            let nextSequenceNumber = sequenceNumber.next
            XCTAssert(sequenceNumber.isLessThan(nextSequenceNumber), "sequence number looping is bad")
            XCTAssert(nextSequenceNumber.isGreaterThan(sequenceNumber), "sequence number looping is bad")
            XCTAssert(nextSequenceNumber != sequenceNumber, "sequence number looping is bad")
            sequenceNumber = nextSequenceNumber
        }
        
        XCTAssert(RTPSequenceNumber(UInt16.max - UInt16(32)).isGreaterThan(RTPSequenceNumber(32)), "sequence number looping is bad")
        XCTAssert(RTPSequenceNumber(UInt16.max - UInt16(2)).isLessThan(RTPSequenceNumber(2)), "sequence number looping is bad")
    }
    
    func testKeepAlive() {
        do {
            let keepAlivePacket = session.archiver.keepAlivePacket
            let archive = try session.unarchiver.unarchive(packet: keepAlivePacket)
            guard case .keepAlive = archive else {
                XCTFail("unexpected unarchived result")
                return
            }
        } catch (let error) {
            XCTFail("\(error)")
        }
    }
    
    func testRealTimePingPong() {
        do {
            let pingPacket = session.archiver.pingPacket
            Thread.sleep(forTimeInterval: 0.1)
            let pingThing = try session.unarchiver.unarchive(packet: pingPacket)
            guard case .ping(let timestamp) = pingThing else {
                XCTFail("unexpected unarchived result")
                return
            }
            
            let pongPacket = session.archiver.pongPacket(withTimestamp: timestamp)
            Thread.sleep(forTimeInterval: 0.1)
            let pongThing = try session.unarchiver.unarchive(packet: pongPacket)
            guard case .pong(let duration) = pongThing else {
                XCTFail("unexpected unarchived result")
                return
            }
            XCTAssert(duration >= 200 && duration <= 400, "duration is weird (\(duration))")
        } catch (let error) {
            XCTFail("\(error)")
        }
    }
    
    func testStream() {
        do {
            let testString = "this is a test."
            guard let payload = testString.data(using: .utf8) else {
                XCTFail("failed to archive string")
                return
            }
            let streamPacket = session.archiver.streamPacket(for: StreamData(version: 1, payload: payload))
            let archive = try session.unarchiver.unarchive(packet: streamPacket)
            guard case .stream(let streamData) = archive else {
                XCTFail("unexpected unarchived result")
                return
            }
            
            guard let afterString = String(data: streamData.payload, encoding: .utf8) else {
                XCTFail("failed to unarchive string")
                return
            }
            
            XCTAssert(testString == afterString, "test string do not match")
        } catch (let error) {
            XCTFail("\(error)")
        }
    }

    func testRealTimeMessage1() {
        do {
            let messageType: UInt8 = 33
            let testString = "this is a test."
            guard let writeData = testString.data(using: .utf8) else {
                XCTFail("failed to archive string")
                return
            }
            
            let beforeMessage = Message(type: messageType, payload: writeData)
            let beforeArchive = session.archiver.messagePackets(forMessage: beforeMessage)
            XCTAssert(beforeArchive.count == 1, "incorrect packet count")
            
            let data = beforeArchive[0]
            let afterArchive = try session.unarchiver.unarchive(packet: data)
            guard case .message(let message) = afterArchive else {
                XCTFail("unexpected unarchived result")
                return
            }
            
            XCTAssert(message.type == messageType, "message.type is unexpected value")
            guard let payload = message.payload, let afterString = String(data: payload, encoding: .utf8) else {
                XCTFail("failed to unarchive string")
                return
            }
            
            XCTAssert(testString == afterString, "test string do not match")
        } catch (let error) {
            XCTFail("\(error)")
        }
    }
    
    func testRealTimeMessageN() {
        do {
            let messageType: UInt8 = 33
            let testString = "this is a test.this is a test.this is a test.this is a test.this is a test.this is a test.this is a test.this is a test.this is a test.this is a test.this is a test.this is a test.this is a test.this is a test.this is a test.this is a test.this is a test.this is a test.this is a test.this is a test.this is a test.this is a test.this is a test.this is a test.this is a test.this is a test.this is a test.this is a test.this is a test.this is a test.this is a test.this is a test.this is a test.this is a test.this is a test.this is a test.this is a test.this is a test.this is a test.this is a test.this is a test.this is a test.this is a test.this is a test.this is a test.this is a test.this is a test.this is a test.this is a test.this is a test.this is a test.this is a test.this is a test.this is a test.this is a test.this is a test.this is a test.this is a test.this is a test.this is a test.this is a test.this is a test.this is a test.this is a test.this is a test.this is a test.this is a test.this is a test.this is a test.this is a test.this is a test.this is a test.this is a test.this is a test.this is a test.this is a test.this is a test.this is a test.this is a test.this is a test.this is a test.this is a test.this is a test.this is a test.this is a test.this is a test.this is a test.this is a test.this is a test.this is a test.this is a test.this is a test.this is a test.this is a test.this is a test.this is a test.this is a test.this is a test.this is a test.this is a test.this is a test.this is a test.this is a test.this is a test.this is a test.this is a test.this is a test.this is a test.this is a test.this is a test.this is a test.this is a test.this is a test.this is a test.this is a test.this is a test.this is a test.this is a test.this is a test.this is a test.this is a test.this is a test.this is a test.this is a test.this is a test.this is a test.this is a test.this is a test.this is a test.this is a test.this is a test.this is a test.this is a test.this is a test.this is a test.this is a test.this is a test.this is a test.this is a test.this is a test.this is a test.this is a test.this is a test.this is a test.this is a test.this is a test.this is a test.this is a test.this is a test.this is a test.this is a test.this is a test.this is a test.this is a test.this is a test.this is a test.this is a test.this is a test.this is a test.this is a test.this is a test.this is a test.this is a test.this is a test.this is a test.this is a test.this is a test.this is a test.this is a test.this is a test.this is a test.this is a test.this is a test.this is a test.this is a test.this is a test.this is a test."
            guard let writeData = testString.data(using: .utf8) else {
                XCTFail("failed to archive string")
                return
            }
            
            let beforeMessage = Message(type: messageType, payload: writeData)
            let beforeArchive = session.archiver.messagePackets(forMessage: beforeMessage)
            XCTAssert(beforeArchive.count == 3, "incorrect packet count")
            
            let archive0 = try session.unarchiver.unarchive(packet: beforeArchive[0])
            guard case .messageFragment = archive0 else {
                XCTFail("unexpected unarchived result")
                return
            }
            
            let archive1 = try session.unarchiver.unarchive(packet: beforeArchive[1])
            guard case .messageFragment = archive1 else {
                XCTFail("unexpected unarchived result")
                return
            }
            
            let archive2 = try session.unarchiver.unarchive(packet: beforeArchive[2])
            guard case .message(let message) = archive2 else {
                XCTFail("unexpected unarchived result")
                return
            }
            
            XCTAssert(message.type == messageType, "message.type is unexpected value")
            guard let payload = message.payload, let afterString = String(data: payload, encoding: .utf8) else {
                XCTFail("failed to unarchive string")
                return
            }
            
            XCTAssert(testString == afterString, "test string do not match")
        } catch (let error) {
            XCTFail("\(error)")
        }
    }
    
}
