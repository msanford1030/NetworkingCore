//
//  StackTests.swift
//
//  Created by Michael Sanford on 9/16/16.
//  Copyright © 2017 flipside5. All rights reserved.
//

import XCTest
import NetworkingCore

class StackTests: XCTestCase {
    
    func testPushPop() {
        var stack = Stack<Int>()
        stack.add(1)
        stack.add(2)
        stack.add(3)
        
        XCTAssertTrue(stack.count == 3)
        
        XCTAssertTrue(stack.remove() == 3)
        XCTAssertTrue(stack.remove() == 2)
        XCTAssertTrue(stack.remove() == 1)
        
        XCTAssertTrue(stack.count == 0)
    }
    
    func testOrderOfItems() {
        var stack = Stack<Int>()
        stack.add(1)
        stack.add(2)
        stack.add(3)
        
        XCTAssertTrue(stack.ordered == [3,2,1])
        
        XCTAssert(stack.unordered.contains(1))
        XCTAssert(stack.unordered.contains(2))
        XCTAssert(stack.unordered.contains(3))
    }
    
    func testLotsOfPushPops() {
        var stack = Stack<Int>()
        
        let innerStep = 10
        for _ in 0 ..< 100000 {
            for pushStep in 0 ..< innerStep {
                stack.add(pushStep)
            }
            XCTAssert(stack.count == innerStep)
            for _ in 0 ..< innerStep {
                _ = stack.remove()
            }
            XCTAssert(stack.count == 0 && stack.isEmpty)
        }
    }
    
    func testPop() {
        var stack = Stack<Int>()
        
        XCTAssertTrue(stack.isEmpty)
        XCTAssertTrue(stack.remove() == nil)
        XCTAssertTrue(stack.first == nil)
        
        stack.add(1)
        XCTAssertTrue(stack.first == 1)
        XCTAssertTrue(!stack.isEmpty)
        
        stack.add(2)
        XCTAssertTrue(stack.first == 2)
        XCTAssertTrue(!stack.isEmpty)
        
        stack.add(3)
        XCTAssertTrue(stack.first == 3)
        XCTAssertTrue(!stack.isEmpty)
        
        XCTAssertTrue(stack.remove() == 3)
        XCTAssertTrue(stack.first == 2)
        XCTAssertTrue(!stack.isEmpty)
        
        XCTAssertTrue(stack.remove() == 2)
        XCTAssertTrue(stack.first == 1)
        XCTAssertTrue(!stack.isEmpty)
        
        XCTAssertTrue(stack.remove() == 1)
        
        XCTAssertTrue(stack.isEmpty)
        XCTAssertTrue(stack.first == nil)
        XCTAssertTrue(stack.remove() == nil)
        XCTAssertTrue(stack.isEmpty)
    }
    
    func testInit() {
        var stack = Stack<Int>([1, 2, 3])
        XCTAssertTrue(stack.count == 3)
        XCTAssertTrue(stack.ordered == [3,2,1])
        XCTAssertTrue(stack.remove() == 3)
        XCTAssertTrue(stack.remove() == 2)
        XCTAssertTrue(stack.remove() == 1)
        XCTAssertTrue(stack.remove() == nil)
    }
    
    func testImmutables() {
        let stack = Stack<Int>([1, 2, 3])
        
        let (stack2, item) = stack.removed()
        XCTAssertTrue(stack.count == 3)
        XCTAssertTrue(stack2.count == 2)
        XCTAssertTrue(stack2.ordered == [2, 1])
        XCTAssertTrue(item == 3)
        
        let stack3 = stack2.added(4)
        XCTAssertTrue(stack2.count == 2)
        XCTAssertTrue(stack3.count == 3)
        XCTAssertTrue(stack3.ordered == [4, 2, 1])
        
        let stack4 = Stack<Int>()
        XCTAssertTrue(stack4.count == 0)
        let (stack5, item5) = stack4.removed()
        XCTAssertTrue(stack5.count == 0)
        XCTAssertTrue(item5 == nil)
        
    }
}
