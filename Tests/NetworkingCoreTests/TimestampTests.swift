//
//  TimestampTests.swift
//
//  Created by Michael Sanford on 9/22/16.
//  Copyright © 2017 flipside5. All rights reserved.
//

import Foundation
import XCTest
import NetworkingCore

class TimestampTests: XCTestCase {
    
    func testInits() {
        let beforeTimestamp = Date.now
        
        for _ in 0...1000000 {
            let now1 = Date()
            let now2 = Date(timestamp: Date.now)
            let now3 = Date(timestamp: now1.timestamp)

            XCTAssertTrue(now1.timestamp == now3.timestamp)
            XCTAssertTrue(now2.timestamp >= now1.timestamp)
            
            XCTAssertTrue(abs(now1.timeIntervalSince(now3)) < Timestamp64.ulpOfOne)
            XCTAssertTrue(abs(now3.timeIntervalSince(now1)) < Timestamp64.ulpOfOne)
            XCTAssertTrue((now2.timeIntervalSinceReferenceDate > now1.timeIntervalSinceReferenceDate) || ((now1.timeIntervalSinceReferenceDate - now2.timeIntervalSinceReferenceDate) < (Timestamp64.ulpOfOne / 2.0)))
        }
        
        let afterTimestamp = Date.now
        
        XCTAssertTrue(beforeTimestamp < afterTimestamp)
        
        let testDuration = Date.timeInterval(between: beforeTimestamp, and: afterTimestamp)
        let testDuration2 = Date.timeInterval(between: afterTimestamp, and: beforeTimestamp)
        let duration = Date(timestamp: afterTimestamp).timeIntervalSince(Date(timestamp: beforeTimestamp))
        XCTAssertTrue(testDuration == duration)
        XCTAssertTrue(testDuration == testDuration2)
    }
}
