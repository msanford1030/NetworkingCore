import XCTest
@testable import NetworkingCoreTests

XCTMain([
    testCase(NetworkingCoreTests.allTests),
])
