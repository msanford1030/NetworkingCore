//
//  BasicLogger.swift
//
//  Created by Michael Sanford on 5/21/16.
//  Copyright © 2017 flipside5. All rights reserved.
//

import Foundation

public enum LogLevel: Int, CustomStringConvertible {
    case debug = 0
    case info = 1
    case warning = 2
    case error = 3
    
    public var description: String {
        switch self {
        case .debug: return "debug"
        case .info: return "info"
        case .warning: return "warning"
        case .error: return "error"
        }
    }
}

public protocol LoggerProtocol {
    var logLevel: LogLevel { get set }
    func log(_ logLevel: LogLevel, domain: @autoclosure() -> String, log: @autoclosure() -> String)
}

public var currentLogger: LoggerProtocol = FileLogger.default {
    didSet {
        log = currentLogger.log
        currentLogger.logLevel = logLevel
    }
}
public private(set) var log: ((LogLevel, @autoclosure() -> String, @autoclosure() -> String) -> Void) = FileLogger.default.log
public var logLevel: LogLevel = .warning {
    didSet {
        currentLogger.logLevel = logLevel
    }
}

public class BasicLogger: LoggerProtocol {
    public static let shared = BasicLogger()
    
    public var logLevel = LogLevel.warning
    
    public func log(_ logLevel: LogLevel, domain: @autoclosure() -> String, log: @autoclosure() -> String) {
        guard logLevel.rawValue >= self.logLevel.rawValue else { return }
        print("\(Date.now32): [\(domain())] \(log())")
    }
}
