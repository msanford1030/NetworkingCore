//
//  Stream.swift
//
//  Created by Michael Sanford on 12/27/16.
//  Copyright © 2017 flipside5. All rights reserved.
//

import Foundation

public struct StreamData: ByteCoding {
    public let version: Int
    public let payload: Data
    
    public init(version: Int, payload: Data) {
        self.version = version
        self.payload = payload
    }
    
    public func encode(with archiver: ByteArchiver) {
        guard version <= Int(UInt8.max) else {
            assert(false, "invalid stream version number of encoding")
            return
        }
        let encodedVersion = UInt8(version)
        archiver.encode(encodedVersion)
        archiver.encode(payload)
    }
    
    public init?(unarchiver: ByteUnarchiver) {
        do {
            version = Int(try unarchiver.decodeUInt8())
            payload = try unarchiver.decodeData()
        } catch {
            return nil
        }
    }
}
