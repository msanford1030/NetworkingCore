//
//  ByteUnarchiver.swift
//
//  Created by Michael Sanford on 1/20/17.
//  Copyright © 2017 flipside5. All rights reserved.
//

import Foundation

public enum ByteUnarchiverError: Error {
    case invalidInternalFormat
}

public class ByteUnarchiver {
    
    private let archive: Data
    private var ptrIndex: Int
    
    public init(archive: Data) {
        self.archive = archive
        ptrIndex = 0
    }
    
    public var remainingArchive: Data {
        return archive.subdata(in: ptrIndex ..< archive.count)
    }
    
    public func decodeString(asAscii: Bool = true) throws -> String {
        let data: Data = try decodeData()
        guard data.count > 0 else { return "" }
        guard let string = String(data: data, encoding: asAscii ? .utf8 : .utf16) else { throw ByteUnarchiverError.invalidInternalFormat }
        return string
    }
    
    public func decodeBool() throws -> Bool {
        return try decodeValue()
    }
    
    public func decodeBool() throws -> Bool? {
        let rawValue: UInt8 = try decodeUInt8()
        guard rawValue != UInt8.max else { return nil }
        return rawValue != 0
    }
    
    public func decodeUInt8() throws -> UInt8 {
        return try decodeValue()
    }
    
    public func decodeUInt8() throws -> UInt8? {
        let rawValue: UInt16 = try decodeUInt16()
        guard rawValue != UInt16.max else { return nil }
        return UInt8(rawValue)
    }
    
    public func decodeUInt16() throws -> UInt16 {
        return try decodeValue()
    }
    
    public func decodeUInt32() throws -> UInt32 {
        return try decodeValue()
    }
    
    public func decodeUInt64() throws -> UInt64 {
        return try decodeValue()
    }
    
    public func decodeInt() throws -> Int {
        return try decodeValue()
    }
    
    private func decodeData(count: UInt16) -> Data {
        let length = Int(count)
        guard length > 0 else { return Data() }
        let data = remainingArchive.subdata(in: 0 ..< length)
        ptrIndex += length
        return data
    }
    
    public func decodeData() throws -> Data {
        return decodeData(count: try decodeUInt16())
    }
    
    public func decodeData() throws -> Data? {
        let count: UInt16 = try decodeUInt16()
        guard count != UInt16.max else { return nil }
        return decodeData(count: count)
    }
    
    private func decodeValue<T>() throws -> T {
        var value: T?
        remainingArchive.withUnsafeBytes { ( ptr: UnsafePointer<T>) in value = ptr.pointee }
        ptrIndex += MemoryLayout<T>.size
        return value!
    }
}
