//
//  RTPPacket.swift
//
//  Created by Michael Sanford on 12/17/16.
//  Copyright © 2017 flipside5. All rights reserved.
//

import Foundation

typealias RTPPayloadSize = UInt16
public typealias RTPSourceIdentifier = UInt32
public typealias RTPSequenceNumber = UInt16

public let RTPPacketByteSize: Int = RTPPacket.Layout.size
public let RTPPayloadByteSize: Int = RTPPacket.Layout.payloadSize

fileprivate struct RTPPacket {
    struct Layout {
        typealias Info = UInt8
        static let size = headerSize + payloadSize
        static let payloadSize = 900
        
        static let headerSize: Int =
            MemoryLayout<Info>.size +
                MemoryLayout<RTPPacket.PayloadType>.size +
                MemoryLayout<RTPSequenceNumber>.size +
                MemoryLayout<Timestamp32>.size +
                MemoryLayout<RTPSourceIdentifier>.size +
                MemoryLayout<RTPPayloadSize>.size
        
        static let info: Info = 64
    }
    
    enum PayloadType: UInt8 {
        case keepAlive = 80
        case ping = 81
        case pong = 82
        case messageFragment = 83
        case stream = 84
        case disconnect = 85
        case connect = 86
    }

    let version: UInt8
    let hasExtension: Bool
    let csrcCount: UInt8
    let isMarkerEnabled: Bool
    let ssrc: RTPSourceIdentifier
    let sequenceNumber: RTPSequenceNumber
    let timestamp: Timestamp32
    let payloadType: PayloadType
    let payload: Data
}

enum PacketArchiveError: Error {
    case payloadTooLarge
}

enum PacketUnarchiveError: Error {
    case invalidPacketSize
    case internalError
}

/*------------------------------------------------*/

fileprivate class RTPPacketArchiver {
    let ssrc: RTPSourceIdentifier
    
    init(ssrc: RTPSourceIdentifier) {
        self.ssrc = ssrc
    }

    private func header(withSequenceNumber sequenceNumber: RTPSequenceNumber, timestamp: Timestamp32,
                        payloadType: RTPPacket.PayloadType, payloadSize: Int) -> Data {
        let archiver = ByteArchiver(capacity: RTPPacket.Layout.headerSize)
        archiver.encode(RTPPacket.Layout.info)
        archiver.encode(payloadType.rawValue)
        archiver.encode(sequenceNumber)
        archiver.encode(timestamp)
        archiver.encode(ssrc)
        let archivedPayloadSize = UInt16(payloadSize)
        archiver.encode(archivedPayloadSize)
        return archiver.archive
    }
    
    func archive(withSequenceNumber sequenceNumber: RTPSequenceNumber, timestamp: Timestamp32,
                 payloadType: RTPPacket.PayloadType, payload: Data) throws -> Data {
        guard payload.count <= RTPPacket.Layout.payloadSize else { throw PacketArchiveError.payloadTooLarge }
        var packet = Data(capacity: RTPPacket.Layout.size)
        packet.append(header(withSequenceNumber: sequenceNumber, timestamp: timestamp, payloadType: payloadType, payloadSize: payload.count))
        packet.append(payload)
        
        let payloadPaddingSize = RTPPacket.Layout.payloadSize - payload.count
        if payloadPaddingSize > 0 {
            packet.append(Data(count: payloadPaddingSize))
        }
        
        assert(packet.count == RTPPacket.Layout.size, "archive packet size is incorrect")
        return packet
    }
    
    func archive(
        sequenceNumber: RTPSequenceNumber,
        timestamp: Timestamp32,
        payloadType: RTPPacket.PayloadType) -> Data {
        
        var packet = Data(capacity: RTPPacket.Layout.size)
        packet.append(header(withSequenceNumber: sequenceNumber, timestamp: timestamp, payloadType: payloadType, payloadSize: 0))
        packet.append(Data(count: RTPPacket.Layout.payloadSize))
        
        return packet
    }
    
}

/*------------------------------------------------*/

fileprivate class RTPPacketUnarchiver {
    
    func unarchive(packet: Data) throws -> RTPPacket {
        guard packet.count == RTPPacket.Layout.size else { throw PacketUnarchiveError.invalidPacketSize }
        let unarchiver = ByteUnarchiver(archive: packet)
        
        let _: RTPPacket.Layout.Info = try unarchiver.decodeUInt8()
        guard let payloadType = RTPPacket.PayloadType(rawValue: try unarchiver.decodeUInt8()) else { throw PacketUnarchiveError.internalError }
        let sequenceNumber: RTPSequenceNumber = try unarchiver.decodeUInt16()
        let timestamp: Timestamp32 = try unarchiver.decodeUInt32()
        let ssrc: RTPSourceIdentifier = try unarchiver.decodeUInt32()
        let payloadSize = Int(try unarchiver.decodeUInt16())
        let payload = unarchiver.remainingArchive.subdata(in: 0 ..< payloadSize)
        
        return RTPPacket(
            version: 2,
            hasExtension: false,
            csrcCount: 0,
            isMarkerEnabled: false,
            ssrc: ssrc,
            sequenceNumber: sequenceNumber,
            timestamp: timestamp,
            payloadType: payloadType,
            payload: payload)
    }
}

/*------------------------------------------------*/

public class RTPArchiver {
    public let ssrc: RTPSourceIdentifier
    private(set) var sequenceNumber: RTPSequenceNumber
    private let rtpArchiver: RTPPacketArchiver
    private let clock: Clock
    private var messageID: UInt8 = 0

    public init(ssrc: RTPSourceIdentifier, clock: Clock) {
        self.ssrc = ssrc
        self.clock = clock
        sequenceNumber = UInt16(UInt8.random(UInt8.max))
        rtpArchiver = RTPPacketArchiver(ssrc: ssrc)
    }
    
    private func packet(withPayloadType payloadType: RTPPacket.PayloadType) -> Data {
        let packetSequenceNumber = sequenceNumber
        sequenceNumber = sequenceNumber.next
        return rtpArchiver.archive(sequenceNumber: packetSequenceNumber, timestamp: clock.now, payloadType: payloadType)
    }
    
    private func packet(withPayloadType payloadType: RTPPacket.PayloadType, payload: Data, timestamp: Timestamp32? = nil) throws -> Data {
        let packetSequenceNumber = sequenceNumber
        sequenceNumber = sequenceNumber.next
        return try rtpArchiver.archive(withSequenceNumber: packetSequenceNumber, timestamp: timestamp ?? clock.now, payloadType: payloadType, payload: payload)
    }
    
    public var keepAlivePacket: Data {
        return packet(withPayloadType: .keepAlive)
    }
    
    public var connectPacket: Data {
        return packet(withPayloadType: .connect)
    }
    
    public func disconnectPacket(forReason reason: DisconnectReason) -> Data {
        var bytes = reason
        let payload = Data(bytes: &bytes, count: MemoryLayout<DisconnectReason>.size / MemoryLayout<UInt8>.size)
        do {
            return try packet(withPayloadType: .disconnect, payload: payload)
        } catch {
            assert(false, "disconnect packet archive unexpectedly failed")
            return Data()
        }
    }
    
    public var pingPacket: Data {
        return packet(withPayloadType: .ping)
    }
    
    public func pongPacket(withTimestamp timestamp: Timestamp32) -> Data {
        var bytes = timestamp
        let payload = Data(bytes: &bytes, count: MemoryLayout<Timestamp32>.size / MemoryLayout<UInt8>.size)
        do {
            return try packet(withPayloadType: .pong, payload: payload)
        } catch {
            assert(false, "pong packet archive unexpectedly failed")
            return Data()
        }
    }
    
    private func _streamPacket(for payload: Data) -> Data {
        do {
            return try packet(withPayloadType: .stream, payload: payload)
        } catch {
            assert(false, "stream packet archive unexpectedly failed")
            return Data()
        }
    }
    
    public func streamPacket(for streamData: StreamData) -> Data {
        var versionValue = UInt8(streamData.version)
        let versionData = Data(bytes: &versionValue, count: MemoryLayout<UInt8>.size)
        let payload = versionData + streamData.payload
        return _streamPacket(for: payload)
    }
    
    public func messagePackets(forMessage message: Message) -> [Data] {
        
        func headerData(forPart part: UInt8, of count: UInt8) -> Data {
            let archiver = ByteArchiver(capacity: MessageFragment.Layout.headerSize)
            archiver.encode(message.type)
            archiver.encode(messageID)
            archiver.encode(part)
            archiver.encode(count)
            return archiver.archive
        }
        
        func incrementMessageID() {
            messageID = (messageID == UInt8.max) ? 0 : messageID + 1
        }
        
        do {
            let timestamp = clock.now
            
            guard let payload = message.payload else {
                let packet = try self.packet(withPayloadType: .messageFragment, payload: headerData(forPart: 0, of: 1))
                incrementMessageID()
                return [packet]
            }
            
            var packets: [Data] = []
            let fragmentPayloadByteCount = RTPPacket.Layout.payloadSize - MessageFragment.Layout.headerSize
            let fragmentCount = UInt8((payload.count / fragmentPayloadByteCount) + 1)
            let fragmentLastPayloadByteCount = payload.count % fragmentPayloadByteCount
            for fragmentPart in 0 ..< fragmentCount {
                let start = Int(fragmentPart) * fragmentPayloadByteCount
                let end = start + ((fragmentPart + 1 == fragmentCount) ? fragmentLastPayloadByteCount : fragmentPayloadByteCount)
                let fragmentHeader = headerData(forPart: fragmentPart, of: fragmentCount)
                let fragmentPayload = payload.subdata(in: start ..< end)
                let payload = fragmentHeader + fragmentPayload
                
                let packet = try self.packet(withPayloadType: .messageFragment, payload: payload, timestamp: timestamp)
                packets.append(packet)
                sequenceNumber = sequenceNumber.next
            }
            
            incrementMessageID()
            return packets
        } catch {
            assert(false, "stream packet archive unexpectedly failed")
            return []
        }
    }
}

/*------------------------------------------------*/

public class RTPUnarchiver {
    
    private struct FragmentInfo {
        static let maxReceivedCount = 3
        
        let receivedTime: Timestamp64
        var receivedFragmentCount: Int
        var fragments: FixedUnsafeArray<Data>
        
        init?(count: Int) {
            receivedTime = Date.now
            receivedFragmentCount = 1
            guard let fragments = FixedUnsafeArray<Data>(capacity: count) else { return nil }
            self.fragments = fragments
        }
        
        var expectedFragmentCount: Int {
            return fragments.capacity * FragmentInfo.maxReceivedCount
        }
        
        var isStale: Bool {
            return Date.timeInterval(from: receivedTime) > 10
        }
    }
    
    private let rtpUnarchiver = RTPPacketUnarchiver()
    private var fragmentInfos: [UInt8: FragmentInfo] = [:]
    private var mruSequenceNumber: RTPSequenceNumber?
    private let clock: Clock
    
    public init(clock: Clock) {
        self.clock = clock
    }
    
    public func removeStaleMessageFragments() {
        let staleIDs = fragmentInfos.reduce([]) { (result, entry: (messageID: UInt8, fragInfo: FragmentInfo)) -> [UInt8] in
            return entry.fragInfo.isStale ? result + [entry.messageID] : result
        }
        staleIDs.forEach { self.fragmentInfos[$0] = nil }
    }
    
    public func unarchive(packet: Data) throws -> RTPTransportValue {
        
        func unarchiveFragment(from packet: RTPPacket) throws -> MessageFragment {
            let unarchiver = ByteUnarchiver(archive: packet.payload)
            
            let messageType: UInt8 = try unarchiver.decodeUInt8()
            let messageID: UInt8 = try unarchiver.decodeUInt8()
            let fragmentPart: UInt8 = try unarchiver.decodeUInt8()
            let fragmentCount: UInt8 = try unarchiver.decodeUInt8()
            let fragmentPayload = unarchiver.remainingArchive
            
            return MessageFragment(messageType: messageType, messageID: messageID, fragmentPart: fragmentPart,
                                   fragmentCount: fragmentCount, fragmentPayload: fragmentPayload)
        }
        
        let rtpPacket = try rtpUnarchiver.unarchive(packet: packet)
        switch rtpPacket.payloadType {
        case .keepAlive:
            return .keepAlive
        case .connect:
            return .connect
        case .disconnect:
            let unarchiver = ByteUnarchiver(archive: rtpPacket.payload)
            let reason = DisconnectReason(rawValue: try unarchiver.decodeUInt8()) ?? DisconnectReason.unknown
            return .disconnect(reason)
        case .ping:
            return .ping(rtpPacket.timestamp)
        case .pong:
            let unarchiver = ByteUnarchiver(archive: rtpPacket.payload)
            let timestamp: Timestamp32 = try unarchiver.decodeUInt32()
            let duration = clock.now - timestamp
            return .pong(duration)
        case .stream:
            if let mruSequenceNumber = mruSequenceNumber {
                assert(rtpPacket.sequenceNumber != mruSequenceNumber, "received identical packet twice. this should never happen")
                guard rtpPacket.sequenceNumber > mruSequenceNumber else { return .streamDOA }
            }
            mruSequenceNumber = rtpPacket.sequenceNumber
            
            let unarchiver = ByteUnarchiver(archive: rtpPacket.payload)
            let version = Int(try unarchiver.decodeUInt8())
            let streamData = StreamData(version: version, payload: unarchiver.remainingArchive)
            return .stream(streamData)
            
        case .messageFragment:
            let fragment = try unarchiveFragment(from: rtpPacket)
            assert(fragment.fragmentCount > 0, "message fragment count cannot be zero")
            
            if fragment.fragmentCount == 1 {
                if let existingFragInfo = fragmentInfos[fragment.messageID] {
                    var updatedExistingFragInfo = existingFragInfo
                    if updatedExistingFragInfo.receivedFragmentCount == (FragmentInfo.maxReceivedCount - 1) {
                        // received all of them so remove frag info
                        fragmentInfos[fragment.messageID] = nil
                    } else {
                        updatedExistingFragInfo.receivedFragmentCount += 1
                    }
                    return .messageFragmentDOA
                }
                
                removeStaleMessageFragments()
                guard let fragInfo = FragmentInfo(count: 0) else { throw PacketUnarchiveError.internalError }
                fragmentInfos[fragment.messageID] = fragInfo
                return .message(Message(type: fragment.messageType, payload: fragment.fragmentPayload))
            }
            
            let fragmentCount = Int(fragment.fragmentCount)
            guard let previousFragmentInfo = fragmentInfos[fragment.messageID] ?? FragmentInfo(count: fragmentCount) else {
                throw PacketUnarchiveError.internalError
            }

            if previousFragmentInfo.receivedFragmentCount == (previousFragmentInfo.expectedFragmentCount - 1) {
                // received all of them so remove frag info
                fragmentInfos[fragment.messageID] = nil
                return .messageFragmentDOA
            }
            
            var receivedFragmentInfo = previousFragmentInfo
            receivedFragmentInfo.receivedFragmentCount += 1
            fragmentInfos[fragment.messageID] = receivedFragmentInfo
            
            let fragmentPart = Int(fragment.fragmentPart)
            guard receivedFragmentInfo.fragments[fragmentPart] == nil else {
                // ignoring redudant message fragment
                return .messageFragmentDOA
            }
            
            receivedFragmentInfo.fragments[fragmentPart] = fragment.fragmentPayload
            if receivedFragmentInfo.fragments.count == fragmentCount {
                // have all fragments
                let messagePayload = receivedFragmentInfo.fragments.ordered.reduce(Data()) { (result, data) in result + data }
                
                removeStaleMessageFragments()
                return .message(Message(type: fragment.messageType, payload: messagePayload))
            }
            
            fragmentInfos[fragment.messageID] = receivedFragmentInfo
            return .messageFragment
        }
    }
}

/*------------------------------------------------*/

public class RTPSession {
    private(set) var clock: Clock
    public let archiver: RTPArchiver
    public let unarchiver: RTPUnarchiver
    
    public init(ssrc: RTPSourceIdentifier, startClockImmediately: Bool = true) {
        clock = Clock(startImmediately: startClockImmediately)
        archiver = RTPArchiver(ssrc: ssrc, clock: clock)
        unarchiver = RTPUnarchiver(clock: clock)
    }
    
    public func start() {
        clock.start()
    }
    
    public func reset() {
        clock = Clock()
    }
}

/*------------------------------------------------*/

public enum RTPTransportValue {
    case keepAlive
    case connect
    case disconnect(DisconnectReason)
    case ping(Timestamp32) // timestamp = relative time of sender
    case pong(Timestamp32) // timestamp = duration for roundtrip in ms
    case stream(StreamData)
    case streamDOA // stale stream packet
    case message(Message)
    case messageFragment // new message fragment packet
    case messageFragmentDOA // redudant message fragment packet
}

public extension RTPSequenceNumber {
    func isLessThan(_ rhs: RTPSequenceNumber) -> Bool {
        guard self != rhs else { return false }
        return areSequenceValuesLooping(with: self, and: rhs) ? Int(self) > Int(rhs) : Int(self) < Int(rhs)
    }
    
    func isGreaterThan(_ rhs: RTPSequenceNumber) -> Bool {
        guard self != rhs else { return false }
        return areSequenceValuesLooping(with: self, and: rhs) ? Int(self) < Int(rhs) : Int(self) > Int(rhs)
    }
}

private func areSequenceValuesLooping(with value1: RTPSequenceNumber, and value2: RTPSequenceNumber) -> Bool {
    let sequenceNumberTolerance = 16
    return (Int(value1) > (Int(UInt16.max) - sequenceNumberTolerance) && (Int(value2) < sequenceNumberTolerance)) ||
        (Int(value2) > (Int(UInt16.max) - sequenceNumberTolerance) && (Int(value1) < sequenceNumberTolerance))
}

extension RTPSequenceNumber {
    public var next: RTPSequenceNumber {
        let next = self == RTPSequenceNumber.max ? 0 : self + 1
        assert(self.isLessThan(next), "sequence number is horked")
        return next
    }
}

/*-------------------------------------------*/

fileprivate struct MessageFragment {
    struct Layout {
        static let headerSize = MemoryLayout<UInt8>.size + MemoryLayout<UInt8>.size + MemoryLayout<UInt8>.size + MemoryLayout<UInt8>.size
    }
    
    let messageType: UInt8
    let messageID: UInt8
    let fragmentPart: UInt8
    let fragmentCount: UInt8
    let fragmentPayload: Data
}
