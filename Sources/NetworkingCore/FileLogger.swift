//
//  FileLogger.swift
//  TestLogger
//
//  Created by ipad_kid on 7/11/17.
//  Copyright © 2017 flipside5. All rights reserved.
//

import Foundation
import Dispatch

public class FileLogger: LoggerProtocol {
    public static let `default` = FileLogger()
    
    public var logLevel: LogLevel = .warning
    public let path: String
    private let fp: UnsafeMutablePointer<FILE>
    private let queue = DispatchQueue(label: "FileLogger")
    private var flushWorkItem: DispatchWorkItem!
    
    private func write(_ string: String, toFile path: String) {
        if isPrintEnabled {
            print(string)
        }
        
        let logString: String = "\(string)\n"
        let byteArray = Array(logString.utf8)
        
        let expectedCount = logString.utf8.count
        queue.async { [weak self] in
            guard let strongSelf = self else { return }
            let count = fwrite(byteArray, 1, byteArray.count, strongSelf.fp)
            if count != expectedCount {
                print("Error writing \"\(string)\" to \(path)")
            }
        }
    }
    
    #if os(iOS)
    var isPrintEnabled = true
    #else
    var isPrintEnabled = false
    #endif
    
    public init(path: String? = nil) {
        var defaultLogPath: String {
            let logPath: String
            #if os(iOS)
                let appDocuments: [String] = NSSearchPathForDirectoriesInDomains(.documentDirectory, .userDomainMask, true)
                logPath = appDocuments[0]
            #else
                logPath = "/tmp"
            #endif
            return logPath.appending("/default.log")
        }
        
        self.path = path ?? defaultLogPath
        fp = fopen(self.path, "a")
        
        flushWorkItem = queue.addOperation(60, repeats: true) { [weak self] in
            guard let strongSelf = self else { return }
            fflush(strongSelf.fp)
        }
    }
    
    deinit {
        flushWorkItem.cancel()
        fclose(fp)
    }
    
    public func log(_ logLevel: LogLevel, domain: @autoclosure() -> String, log: @autoclosure() -> String) {
        guard logLevel.rawValue >= self.logLevel.rawValue else { return }
        let logString = "\(Date.now32) \(logLevel.description): [\(domain())] \(log())"
        write(logString, toFile: path)
    }
}
