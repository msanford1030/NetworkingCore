//
//  Queue.swift
//
//  Created by Michael Sanford on 9/14/16.
//  Copyright © 2017 flipside5. All rights reserved.
//

import Foundation

public struct Queue<T>: MutableSequence {
    public struct QueueIterator: IteratorProtocol {
        private var currentNode: Node?
        
        fileprivate init(firstNode: Node?) {
            currentNode = firstNode
        }
        
        public mutating func next() -> T? {
            guard let currentNode = self.currentNode else { return nil }
            self.currentNode = currentNode.next
            return currentNode.value
        }
    }
    
    public class Node {
        fileprivate let value: T
        fileprivate var next: Node?
        
        fileprivate init(value: T, next: Node? = nil) {
            self.value = value
            self.next = next
        }
    }
    
    public private(set) var count: Int = 0
    private var headNode: Node? = nil
    private var tailNode: Node? = nil
    
    public init() {}
    
    public mutating func add(_ item: T) {
        guard let tailNode = self.tailNode else {
            headNode = Node(value: item)
            self.tailNode = headNode
            count = 1
            return
        }
        
        let node = Node(value: item)
        tailNode.next = node
        self.tailNode = node
        count += 1
    }
    
    @discardableResult public mutating func remove() -> T? {
        guard let headNode = self.headNode else { return nil }
        count -= 1
        self.headNode = headNode.next
        if self.headNode == nil {
            tailNode = nil
        }
        return headNode.value
    }
    
    public var first: T? {
        return headNode?.value
    }
    
    public var isEmpty: Bool {
        return headNode == nil
    }
    
    /// MARK: Sequence
    public func makeIterator() -> QueueIterator {
        return QueueIterator(firstNode: headNode)
    }
    
    public var underestimatedCount: Int {
        return count
    }
    
    /// MARK: Queue-like API
    public mutating func enqueue(_ item: T) {
        add(item)
    }
    
    @discardableResult public mutating func dequeue() -> T? {
        return remove()
    }
}
