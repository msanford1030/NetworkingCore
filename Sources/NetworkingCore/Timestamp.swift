//
//  Timestamp.swift
//
//  Created by Michael Sanford on 9/22/16.
//  Copyright © 2017 flipside5. All rights reserved.
//

import Foundation

public typealias Timestamp64 = UInt64
public typealias Timestamp32 = UInt32

private let TimestampUnit: TimeInterval = 0.001

private func getTimestamp(fromTimeInterval timeInterval: TimeInterval) -> Timestamp64 {
    return Timestamp64(llround(timeInterval / TimestampUnit))
}

public extension Date {
    static func timeInterval(from then: Timestamp64) -> TimeInterval {
        return timeInterval(between: now, and: then)
    }
    
    static func timeInterval(between timestamp1: Timestamp64, and timestamp2: Timestamp64) -> TimeInterval {
        let timeInterval1 = (TimeInterval(timestamp1) * TimestampUnit)
        let timeInterval2 = (TimeInterval(timestamp2) * TimestampUnit)
        return abs(timeInterval1 - timeInterval2)
    }
    
    static func timeInterval(between timestamp1: Timestamp32, and timestamp2: Timestamp32) -> TimeInterval {
        let timeInterval1 = (TimeInterval(timestamp1) * TimestampUnit)
        let timeInterval2 = (TimeInterval(timestamp2) * TimestampUnit)
        return abs(timeInterval1 - timeInterval2)
    }
    
    static var now: Timestamp64 {
        return getTimestamp(fromTimeInterval: Date.timeIntervalSinceReferenceDate)
    }
    
    static var now32: Timestamp32 {
        return Timestamp32(timestamp: now)
    }
    
    init(timestamp: Timestamp64) {
        self.init(timeIntervalSinceReferenceDate: (TimeInterval(timestamp) * TimestampUnit))
    }
    
    var timestamp: Timestamp64 {
        return getTimestamp(fromTimeInterval: timeIntervalSinceReferenceDate)
    }
}

public extension Timestamp32 {
    init(timestamp: Timestamp64) {
        self.init(timestamp)
    }
    
    init(hours: Int = 0, minutes: Int = 0, seconds: Int = 0) {
        self = hours.hours32 + minutes.minutes32 + seconds.seconds32
    }
    
    var millisecondsSinceNow: Timestamp32 {
        return Date.now32 - self
    }
}

public extension Timestamp64 {
    static var ulpOfOne: TimeInterval = TimestampUnit
    
    init(hours: Int = 0, minutes: Int = 0, seconds: Int = 0) {
        self = hours.hours64 + minutes.minutes64 + seconds.seconds64
    }
    
    var timeIntervalSinceNow: TimeInterval {
        return Date.timeInterval(between: self, and: Date.now)
    }
    
    var millisecondsSinceNow: Timestamp64 {
        return Date.now - self
    }
}

fileprivate extension Int {
    var hours32: Timestamp32 {
        return self.minutes32 * 60
    }
    var minutes32: Timestamp32 {
        return self.seconds32 * 60
    }
    var seconds32: Timestamp32 {
        return Timestamp32(self) * 1000
    }
    
    var hours64: Timestamp64 {
        return self.minutes64 * 60
    }
    var minutes64: Timestamp64 {
        return self.seconds64 * 60
    }
    var seconds64: Timestamp64 {
        return Timestamp64(self) * 1000
    }
}
