//
//  ByteArchiver.swift
//
//  Created by Michael Sanford on 1/20/17.
//  Copyright © 2017 flipside5. All rights reserved.
//

import Foundation

public class ByteArchiver {
    private let domain = "ByteArchiver"
    
    public private(set) var archive: Data
    
    public static func archive(for archivable: ByteCoding) -> Data {
        let archiver = ByteArchiver()
        archivable.encode(with: archiver)
        return archiver.archive
    }
    
    public init(capacity: Int? = nil) {
        if let capacity = capacity {
            archive = Data(capacity: capacity)
        } else {
            archive = Data()
        }
    }
    
    public func encode(_ value: String, asAscii: Bool = true) {
        guard value.count > 0 else {
            encode(Data())
            return
        }
        
        let maxLength = Int(UInt8.max)
        
        func bytesForUTF8(_ string: String) -> [UInt8] {
            return string.utf8.reduce([]) { $0 + [$1] }
        }
        
        func bytesForUTF16(_ string: String) -> [UInt8] {
            return string.utf16.reduce([]) {
                let uInt8Value0 = UInt8($1 >> 8)
                let uInt8Value1 = UInt8($1 & 0x00ff)
                return $0 + [uInt8Value0, uInt8Value1]
            }
        }
        
        var validString = value
        if validString.count > maxLength {
            log(.warning, domain, "truncating string during encode. string\(value)")
            let lastIndex = value.index(value.startIndex, offsetBy: maxLength)
            validString = String(value[..<lastIndex])
        }
        let bytes = asAscii ? bytesForUTF8(validString) : bytesForUTF16(validString)
        let data = Data(bytes: bytes)
        encode(data)
    }
    
    public func encode(_ value: Bool) {
        encodeValue(value: value)
    }
    
    public func encode(_ value: Bool?) {
        guard let rawValue = value else {
            encode(UInt8.max)
            return
        }
        encode(UInt8(rawValue ? 1 : 0))
    }
    
    public func encode(_ value: UInt8) {
        encodeValue(value: value)
    }
    
    public func encode(_ value: UInt8?) {
        guard let rawValue = value else {
            encode(UInt16.max)
            return
        }
        encode(UInt16(rawValue))
    }
    
    public func encode(_ value: UInt16) {
        encodeValue(value: value)
    }
    
    public func encode(_ value: UInt32) {
        encodeValue(value: value)
    }
    
    public func encode(_ value: UInt64) {
        encodeValue(value: value)
    }
    
    public func encode(_ value: Int) {
        encodeValue(value: value)
    }
    
    public func encode(_ data: Data) {
        encode(UInt16(data.count))
        archive.append(data)
    }
    
    public func encode(_ data: Data?) {
        guard let data = data else {
            encode(UInt16.max)
            return
        }
        encode(data)
    }
    
    public func encode(_ value: ByteCoding) {
        value.encode(with: self)
    }
    
    private func encodeValue<T>(value: T) {
        var valueRef = value
        let valueArchive =  Data(bytes: &valueRef, count: MemoryLayout<T>.size)
        archive.append(valueArchive)
    }
}
