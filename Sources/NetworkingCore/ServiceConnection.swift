//
//  ServiceConnection.swift
//
//  Created by Michael Sanford on 10/13/16.
//  Copyright © 2017 flipside5. All rights reserved.
//

import Foundation
import SwiftOnSockets
import Dispatch

public protocol ServiceConnectionDelegate: class {
    func connection(didConnect connection: ServiceConnection)
    func connection(didDisconnect connection: ServiceConnection, error: Error?)
    func connection(_ connection: ServiceConnection, didReceiveMessage: Message)
    func connection(_ connection: ServiceConnection, didReceiveStream: StreamData)
}

public enum ServiceConnectionError: Error {
    case pingAlreadyInProgress
    case internalError
    case lostConnection
}

public enum DisconnectReason: UInt8 {
    case unknown = 0
    
    // from server
    case sessionEnded = 1
    case connectionTimeout = 2
    
    // from client
    case connectionEnded = 3
}

public enum ConnectionStatus {
    case disconnecting
    case disconnected
    case connecting
    case connected
}


private let ServiceConnectionDomain = "ServiceConnection"
private let udpKeepAliveInterval: TimeInterval = 1

/*-----------------------------------------------------------------------------------*/

public class ServiceConnection {
    
    fileprivate enum Event {
        case keepAlive
        case connect
        case disconnect(DisconnectReason)
        case ping(Timestamp32)
        case pong(Timestamp32)
        case message(Message)
        case stream(StreamData)
        case other
    }
    
    fileprivate enum ClientSocket {
        case tcp(TCPClientSocket)
        case udp(UDPClientSocket)
        
        private static let emptyData = Data()
        
        var connectionType: ConnectionType {
            switch self {
            case .tcp:
                return .tcp
            case .udp:
                return .udp
            }
        }
        
        func read(numberOfBytes: Int) throws -> Data? {
            switch self {
            case .tcp(let socket):
                return try socket.read(numberOfBytes: numberOfBytes)
            case .udp(let socket):
                return try socket.receive(numberOfBytes: numberOfBytes)
            }
        }
        
        func read(withSession session: ClientSession) throws -> Event? {
            switch self {
            case .tcp(let socket):
                guard case .tcp(let etpSession) = session else { throw ServiceConnectionError.internalError }
                guard let headerArchive = try socket.read(numberOfBytes: ETPTransportHeader.Layout.size) else { return nil }
                let header = try etpSession.unarchiver.unarchiveHeader(headerArchive)
                
                let transportValue: ETPTransportValue
                if header.payloadSize == 0 {
                    transportValue = try etpSession.unarchiver.unarchiveValue(header: header)
                } else {
                    guard let payload = try socket.read(numberOfBytes: Int(header.payloadSize)) else { return nil }
                    transportValue = try etpSession.unarchiver.unarchiveValue(header: header, payload: payload)
                }
                
                switch transportValue {
                case .keepAlive:
                    return .keepAlive
                case .ping(let timestamp):
                    guard let timestamp = timestamp else { throw ServiceConnectionError.internalError }
                    return .ping(timestamp)
                case .pong(let duration):
                    guard let duration = duration else { throw ServiceConnectionError.internalError }
                    return .pong(duration)
                case .message(let message):
                    return .message(message)
                }
            case .udp(let socket):
                guard case .udp(let rtpSession) = session else { throw ServiceConnectionError.internalError }
                guard let incomingPacket = try socket.receive(numberOfBytes: RTPPacketByteSize) else { return nil }
                let incoming = try rtpSession.unarchiver.unarchive(packet: incomingPacket)
                switch incoming {
                case .keepAlive:
                    return .keepAlive
                case .connect:
                    return .connect
                case .disconnect(let reason):
                    return .disconnect(reason)
                case .ping(let timestamp):
                    return .ping(timestamp)
                case .pong(let duration):
                    return .pong(duration)
                case .message(let message):
                    return .message(message)
                case .stream(let streamData):
                    return .stream(streamData)
                case .messageFragment, .messageFragmentDOA, .streamDOA:
                    return .other
                }
            }
        }
        
        func write(_ data: Data) throws {
            switch self {
            case .tcp(let socket):
                return try socket.write(data)
            case .udp(let socket):
                return try socket.send(data)
            }
        }
        
        func connect() throws {
            switch self {
            case .tcp(let socket):
                try socket.connect()
            case .udp: ()
            }
        }
        
        func close() throws {
            switch self {
            case .tcp(let socket):
                try socket.close()
            case .udp(let socket):
                try socket.close()
            }
        }
        
        var remoteAddress: IPAddress {
            switch self {
            case .tcp(let socket):
                return socket.remoteAddress
            case .udp(let socket):
                return socket.remoteAddress
            }
        }
    }
    
    fileprivate enum ClientSession {
        case tcp(ETPSession)
        case udp(RTPSession)
        
        var keepAliveData: Data {
            switch self {
            case .tcp(let session):
                return session.archiver.keepAliveArchive
            case .udp(let session):
                return session.archiver.keepAlivePacket
            }
        }
        
        var pingData: Data {
            switch self {
            case .tcp(let session):
                return session.archiver.pingArchive
            case .udp(let session):
                return session.archiver.pingPacket
            }
        }
        
        var connectData: Data {
            switch self {
            case .tcp:
                return Data()
            case .udp(let session):
                return session.archiver.connectPacket
            }
        }
        
        func disconnectPacket(withReason reason: DisconnectReason) -> Data {
            switch self {
            case .tcp:
                return Data() // not really needed because this is built into tcp
            case .udp(let session):
                return session.archiver.disconnectPacket(forReason: reason)
            }
        }
        
        func messageData(forMessage message: Message) -> [Data] {
            switch self {
            case .tcp(let session):
                return [session.archiver.messageArchive(withMessage: message)]
            case .udp(let session):
                return session.archiver.messagePackets(forMessage: message)
            }
        }
        
        func streamPacket(for streamData: StreamData) -> Data {
            switch self {
            case .tcp:
                log(.warning, ServiceConnectionDomain, "attempt to stream on event based session")
                return Data()
            case .udp(let session):
                return session.archiver.streamPacket(for: streamData)
            }
        }
        
        func pongData(forTimestamp timestamp: Timestamp32) -> Data {
            switch self {
            case .tcp(let session):
                return session.archiver.pongArchive(withTimestamp: timestamp)
            case .udp(let session):
                return session.archiver.pongPacket(withTimestamp: timestamp)
            }
        }
        
    }
    
    public weak var delegate: ServiceConnectionDelegate?
    
    private let incomingQueue = DispatchQueue.global()
    private let outgoingQueue = DispatchQueue.global()
    private let socket: ClientSocket
    private let session: ClientSession
    private var pingCallback: ((Timestamp32) -> Void)?
    private var udpTimer: DispatchWorkItem?
    private var lastSend = Date.distantPast
    public private(set) var connectionStatus: ConnectionStatus = .disconnected
    private var lastReceived: Date = Date.distantPast
    
    public init?(type: ConnectionType, remoteAddress: IPAddress) {
//        incomingQueue.maxConcurrentOperationCount = 1
//        outgoingQueue.maxConcurrentOperationCount = 1
        switch type {
        case .tcp:
            guard let socket = TCPClientSocket(remoteAddress: remoteAddress) else { return nil }
            self.socket = .tcp(socket)
            self.session = .tcp(ETPSession())
        case .udp:
            guard let socket = UDPClientSocket(remoteAddress: remoteAddress) else { return nil }
            self.socket = .udp(socket)
            self.session = .udp(RTPSession(ssrc: RTPSourceIdentifier(remoteAddress.hashValue)))
        }
    }
    
    public var type: ConnectionType {
        return socket.connectionType
    }
    
    private func onUDPTimerTick() {
        sendKeepAliveIfNeededOnOutgoingQueue()
        disconnectIfNeeded()
    }
    
    private func disconnectIfNeeded() {
        // if connected and have not received anything from the UDP service in 'awhile' than disconnect
        guard case .connected = connectionStatus, abs(lastReceived.timeIntervalSinceNow) > 5 else { return }
        do {
            try disconnect(withError: ServiceConnectionError.lostConnection)
        } catch {
            log(.error, ServiceConnectionDomain, "lost connection internal error. error=\(error)")
        }
    }
    
    private func sendKeepAliveIfNeededOnOutgoingQueue() {
        outgoingQueue.async { [weak self] in
            guard let strongSelf = self else { return }
            do {
                guard abs(strongSelf.lastSend.timeIntervalSinceNow) > (udpKeepAliveInterval - 0.02) else { return }
                try strongSelf.socket.write(strongSelf.session.keepAliveData)
                strongSelf.lastSend = Date()
            } catch (let error) {
                log(.warning, ServiceConnectionDomain, "async sendKeepAliveIfNeededOnOutgoingQueue error (\(error))")
            }
        }
    }
    
    public func connect() throws {
        guard case .disconnected = connectionStatus else { return }
        
        try socket.connect()
        connectionStatus = .connecting
        
        switch session {
        case .tcp:
            onDidConnect()
        case .udp:
            let packet = session.connectData
            try socket.write(packet)
        }
        
        incomingQueue.async { [weak self] in
            
            guard let strongSelf = self else { return }
            do {
                while strongSelf.connectionStatus.isActive, let event = try strongSelf.socket.read(withSession: strongSelf.session) {
                    DispatchQueue.main.async { [weak self] in
                        self?.lastReceived = Date()
                    }
                    switch event {
                    case .keepAlive, .other: ()
                    case .ping(let timestamp):
                        try strongSelf.performOnOutgoingQueue { [weak self] in
                            guard let strongSelf = self else { return }
                            let pongData = strongSelf.session.pongData(forTimestamp: timestamp)
                            try strongSelf.socket.write(pongData)
                        }
                    case .pong(let duration):
                        guard let pingCallback = strongSelf.pingCallback else {
                            assert(false, "ping callback should never be nil on a pong event")
                            return
                        }
                        DispatchQueue.main.async { [weak self] in
                            self?.pingCallback = nil
                            pingCallback(duration)
                        }
                    case .message(let message):
                        DispatchQueue.main.async { [weak delegate = strongSelf.delegate] in
                            delegate?.connection(strongSelf, didReceiveMessage: message)
                        }
                    case .stream(let streamData):
                        DispatchQueue.main.async { [weak delegate = strongSelf.delegate] in
                            delegate?.connection(strongSelf, didReceiveStream: streamData)
                        }
                    case .connect:
                        strongSelf.onDidConnect()
                    case .disconnect:
                        DispatchQueue.main.async { [weak self] in
                            guard let strongSelf = self else { return }
                            do {
                                try strongSelf.disconnect()
                            } catch {
                                log(.warning, ServiceConnectionDomain, "received disconnect command from server. something went wrong.")
                            }
                        }
                    }
                }
            } catch {
                log(.error, ServiceConnectionDomain, "service stopped accepting connections; error=\(error)")
            }
            
            do {
                if strongSelf.connectionStatus.isActive {
                    try strongSelf.disconnect()
                }
            } catch (let error) {
                log(.error, ServiceConnectionDomain, "connection had error on disconnect. error=\(error)")
            }
        }
    }
    
    private func onDidConnect() {
        DispatchQueue.main.async { [weak self] in
            guard let strongSelf = self, case .connecting = strongSelf.connectionStatus else { return }
            strongSelf.connectionStatus = .connected
            strongSelf.lastReceived = Date()
            if case .udp = strongSelf.session {
                strongSelf.udpTimer = DispatchQueue.main.addOperation(udpKeepAliveInterval, repeats: true) { [weak self] in
                    self?.onUDPTimerTick()
                }
            }
            
            strongSelf.delegate?.connection(didConnect: strongSelf)
        }
    }
    
    private func onPong(fromTimestamp timestamp: Timestamp32) {
        let duration = Date.timeInterval(between: timestamp, and: Date.now32) / 1000
        print("ping response time from \(socket.remoteAddress): \(duration)ms")
    }
    
    private func performOnOutgoingQueue(block: @escaping () throws -> Void) throws {
        outgoingQueue.async { [weak self] in
            do {
                try block()
                self?.lastSend = Date()
            } catch (let error) {
                log(.warning, ServiceConnectionDomain, "async operation error (\(error))")
            }
        }
    }
    
    public func sendKeepAlive() throws {
        try performOnOutgoingQueue { [weak self] in
            guard let strongSelf = self else { return }
            try strongSelf.socket.write(strongSelf.session.keepAliveData)
        }
    }
    
    public func sendPing(completion: @escaping (Timestamp32) -> Void) throws {
        guard pingCallback == nil else { throw ServiceConnectionError.pingAlreadyInProgress }
        pingCallback = completion
        try performOnOutgoingQueue { [weak self] in
            guard let strongSelf = self else { return }
            try strongSelf.socket.write(strongSelf.session.pingData)
        }
    }
    
    private func sendDatum(_ datum: [Data]) throws {
        try performOnOutgoingQueue { [weak self] in
            guard let strongSelf = self else { return }
            for data in datum {
                try strongSelf.socket.write(data)
            }
        }
    }
    
    public func send(_ message: Message) throws {
        try performOnOutgoingQueue { [weak self] in
            guard let strongSelf = self else { return }
            let datum = strongSelf.session.messageData(forMessage: message)
            for data in datum { try strongSelf.socket.write(data) }
            
            guard case .udp = strongSelf.type else { return }
            
            DispatchQueue.main.asyncAfter(timeInterval: 0.1) { [weak self] in
                guard let strongSelf = self else { return }
                do { try strongSelf.sendDatum(datum) } catch {}
            }
            
            DispatchQueue.main.asyncAfter(timeInterval: 0.2) { [weak self] in
                guard let strongSelf = self else { return }
                do { try strongSelf.sendDatum(datum) } catch {}
            }
        }
    }
    
    public func send(_ streamData: StreamData) throws {
        try performOnOutgoingQueue { [weak self] in
            guard let strongSelf = self else { return }
            let packet = strongSelf.session.streamPacket(for: streamData)
            try strongSelf.socket.write(packet)
        }
    }
    
    public func disconnect(withError error: Error? = nil) throws {
        guard case .connected = connectionStatus else { return }
        
        connectionStatus = .disconnecting
        udpTimer?.cancel()
        udpTimer = nil
        
        try performOnOutgoingQueue { [weak self] in
            guard let strongSelf = self else { return }
            try strongSelf.socket.write(strongSelf.session.disconnectPacket(withReason: .connectionEnded))
            try strongSelf.socket.close()
            DispatchQueue.main.async { [weak self] in
                guard let strongSelf = self else { return }
                strongSelf.connectionStatus = .disconnected
                strongSelf.delegate?.connection(didDisconnect: strongSelf, error: error)
            }
        }
    }
}

/*-------------------------------------*/

fileprivate extension ConnectionStatus {
    
    var isActive: Bool {
        switch self {
        case .disconnected, .disconnecting: return false
        case .connected, .connecting: return true
        }
    }
}
