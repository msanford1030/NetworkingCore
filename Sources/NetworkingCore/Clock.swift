//
//  Clock.swift
//
//  Created by Michael Sanford on 12/16/16.
//  Copyright © 2017 flipside5. All rights reserved.
//

import Foundation

/// Supports time up to ~49.7 days

public class Clock {
    
    public enum State {
        case inactive, active, paused
    }
    
    private enum InteralState {
        case active(startTime: Timestamp64)
        case paused(startTime: Timestamp64, pausedTime: Timestamp64)
        case inactive
        
        var state: State {
            switch self {
            case .active: return .active
            case .paused: return .paused
            case .inactive: return .inactive
            }
        }
        
        var now: Timestamp64? {
            switch self {
            case .active(let startTime): return Date.now - startTime
            case .paused(let startTime, let pausedTime): return pausedTime - startTime
            case .inactive: return nil
            }
        }
    }
    
    private var internalState: InteralState = .inactive
    
    public init(startImmediately: Bool = false) {
        if startImmediately {
            start()
        }
    }
    
    public var now: Timestamp32 {
        guard let timestamp = internalState.now else {
            assert(false, "unexpected request for 'now' when clock is inactive")
            return 0
        }
        return Timestamp32(timestamp: timestamp)
    }
    
    public var state: State {
        return internalState.state
    }
    
    public func start() {
        guard case .inactive = state else { return }
        internalState = .active(startTime: Date.now)
    }
    
    public func stop() {
        internalState = .inactive
    }
    
    public func pause() {
        guard case .active(let startTime) = internalState else { return }
        internalState = .paused(startTime: startTime, pausedTime: Date.now)
    }
    
    public func unpause() {
        guard case .paused(let startTime, let pausedTime) = internalState else { return }
        internalState = .active(startTime: startTime + (Date.now - pausedTime))
    }
}
