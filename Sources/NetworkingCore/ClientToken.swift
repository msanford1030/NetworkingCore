//
//  ClientToken.swift
//
//  Created by Michael Sanford on 12/21/16.
//  Copyright © 2017 flipside5. All rights reserved.
//

import Foundation
import SwiftOnSockets

public struct ClientToken: Hashable {
    let identifier: UUID
    public let remoteAddress: IPAddress
    let startTimestamp: Timestamp64
    
    public init(remoteAddress: IPAddress) {
        identifier = UUID()
        startTimestamp = Date.now
        self.remoteAddress = remoteAddress
    }
    
    public var hashValue: Int {
        return identifier.hashValue
    }
    
    public var uptime: TimeInterval {
        return Date.timeInterval(between: Date.now, and: startTimestamp)
    }
}

public func ==(lhs: ClientToken, rhs: ClientToken) -> Bool {
    return lhs.identifier == rhs.identifier
}

extension ClientToken: CustomDebugStringConvertible, CustomStringConvertible {
    public var description: String {
        return "\(remoteAddress).\(identifier)"
    }
    
    public var debugDescription: String {
        return description
    }
}
