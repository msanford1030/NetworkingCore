//
//  MutableSequence.swift
//
//  Created by Michael Sanford on 12/18/16.
//  Copyright © 2017 flipside5. All rights reserved.
//

import Foundation

public protocol MutableSequence: Sequence {
    associatedtype ItemType
    
    init()
    
    var count: Int { get }
    var first: ItemType? { get }
    
    mutating func add(_: ItemType)
    @discardableResult mutating func remove() -> ItemType?
}

public extension MutableSequence {
    
    public init(_ item: ItemType) {
        self.init()
        add(item)
    }
    
    public init(_ items: [ItemType]) {
        self.init()
        for item in items {
            add(item)
        }
    }
    
    var isEmpty: Bool {
        return count == 0
    }
    
    var ordered: [ItemType] {
        var array: [ItemType] = []
        
        forEach {
            array.append($0 as! ItemType)
        }
        
        return array
    }
    
    var unordered: [ItemType] {
        return ordered
    }

    func added(_ item: ItemType) -> Self {
        var addedCollection = self
        addedCollection.add(item)
        return addedCollection
    }
    
    func removed() -> (Self, ItemType?) {
        var removedCollection = self
        let item = removedCollection.remove()
        return (removedCollection, item)
    }
}
