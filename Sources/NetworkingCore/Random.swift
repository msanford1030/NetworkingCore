//
//  Random.swift
//  NetworkingCore
//
//  Created by Michael Sanford on 7/9/17.
//
//

import Foundation
#if os(Linux)
    import SwiftGlibc
#endif

public extension UInt32 {
    public static func random(_ max: UInt32) -> UInt32 {
        #if os(Linux)
        let trueMax = max == UInt32.max ? Int32.max : Int32(max)
        return UInt32(SwiftGlibc.rand() % trueMax)
        #else
        return arc4random_uniform(max)
        #endif
    }
    
    public static func random(in range: Range<UInt32>) -> UInt32 {
        guard range.count > 0 else { return 0 }
        let number = random(UInt32(range.count))
        return number + range.lowerBound
    }
}

public extension UInt16 {
    public static func random(_ max: UInt16) -> UInt16 {
        return UInt16(UInt32.random(UInt32(max)))
    }
    
    public static func random(in range: Range<UInt16>) -> UInt16 {
        let baseLower = UInt32(range.lowerBound)
        let baseUpper = UInt32(range.upperBound)
        let baseRange = Range<UInt32>(uncheckedBounds: (lower: baseLower, upper: baseUpper))
        return UInt16(UInt32.random(in: baseRange))
    }
}

public extension UInt8 {
    public static func random(_ max: UInt8) -> UInt8 {
        return UInt8(UInt32.random(UInt32(max)))
    }
    
    public static func random(in range: Range<UInt8>) -> UInt8 {
        let baseLower = UInt32(range.lowerBound)
        let baseUpper = UInt32(range.upperBound)
        let baseRange = Range<UInt32>(uncheckedBounds: (lower: baseLower, upper: baseUpper))
        return UInt8(UInt32.random(in: baseRange))
    }
}

public extension Float {
    static private let precision: Float = 10
    
    public static func random(_ max: Float) -> Float {
        return Float(UInt32.random(UInt32(max * precision))) / precision
    }
    
    public static func random(in range: Range<Float>) -> Float {
        let lower = UInt32(min(UInt64(range.lowerBound * precision), UInt64(UInt32.max)))
        let upper = UInt32(min(UInt64(range.upperBound * precision), UInt64(UInt32.max)))
        let baseRange = Range<UInt32>(uncheckedBounds: (lower: lower, upper: upper))
        let result = Float(UInt32.random(in: baseRange)) / precision
        return result
    }
}

public extension Double {
    static private let precision: Double = 10
    
    public static func random(_ max: Double) -> Double {
        return Double(UInt32.random(UInt32(max * precision))) / precision
    }
    
    public static func random(in range: Range<Double>) -> Double {
        let precision: Double = 10
        let lower = UInt32(min(UInt64(range.lowerBound * precision), UInt64(UInt32.max)))
        let upper = UInt32(min(UInt64(range.upperBound * precision), UInt64(UInt32.max)))
        let baseRange = Range<UInt32>(uncheckedBounds: (lower: lower, upper: upper))
        let result = Double(UInt32.random(in: baseRange)) / precision
        return result
    }
}
