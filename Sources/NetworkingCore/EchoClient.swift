//
//  EchoClient.swift
//
//  Created by Michael Sanford on 10/10/16.
//  Copyright © 2017 flipside5. All rights reserved.
//

import Foundation
import SwiftOnSockets

let serverPort: PortID = 8083

let testString = "this is a test. this is a test. this is a test. this is a test. this is a test. this is a test. this is a test. this is a test. this is a test. this is a test. this is a test. this is a test. this is a test. this is a test. this is a test. this is a test. this is a test. this is a test. this is a test. this is a test. this is a test. this is a test. this is a test. this is a test. this is a test. this is a test. this is a test. this is a test. this is a test. this is a test. this is a test. this is a test. this is a test. this is a test. this is a test. this is a test. this is a test. this is a test. this is a test. this is a test. this is a test. this is a test. this is a test. this is a test. this is a test. this is a test. this is a test. this is a test. this is a test. this is a test. this is a test. this is a test. this is a test. this is a test. this is a test. this is a test. this is a test. this is a test. this is a test. this is a test. this is a test. this is a test. this is a test. this is a test. this is a test. this is a test. this is a test. this is a test. this is a test. this is a test. this is a test. this is a test. this is a test. this is a test. this is a test. this is a test. this is a test. this is a test. this is a test. this is a test. "

public class EchoClient {
    
    private var connection: ServiceConnection?
    
    public init() {}
    
    public func startUsingTCPService1() throws {
        do {
            let serverAddress = IPAddress.localhost(withPort: serverPort)
            guard let clientSocket = TCPClientSocket(remoteAddress: serverAddress) else { throw SocketError.internalError }
            try clientSocket.connect()
            
            let archiver = ETPArchiver()
            let unarchiver = ETPUnarchiver()
            
            guard let writeData = testString.data(using: .utf8) else { throw SocketError.internalError }
            let outgoingMessage = Message(type: 0, payload: writeData)
            
            let archive = archiver.messageArchive(withMessage: outgoingMessage)
            try clientSocket.write(archive)
            
            guard let headerArchive = try clientSocket.read(numberOfBytes: ETPTransportHeader.Layout.size) else { throw SocketError.internalError }
            let header = try unarchiver.unarchiveHeader(headerArchive)
            
            let transportValue: ETPTransportValue
            if header.payloadSize == 0 {
                transportValue = try unarchiver.unarchiveValue(header: header)
            } else {
                guard let payload = try clientSocket.read(numberOfBytes: Int(header.payloadSize)) else { throw SocketError.internalError }
                transportValue = try unarchiver.unarchiveValue(header: header, payload: payload)
            }
            
            switch transportValue {
            case .message(let message):
                guard let payload = message.payload else { print("echo=<ERROR: missing payload>"); return }
                let echo = String(data: payload, encoding: .utf8)
                print("echo=\(echo ?? "<nil>")")
            default:
                print("echo=<ERROR: invalid type>")
            }
        } catch (let error) {
            print("error=\(error)")
            throw error
        }
    }
    
    
    public func startUsingTCPService2() throws {
        class ConnectionDelegate: ServiceConnectionDelegate {
            func connection(_ connection: ServiceConnection, didReceiveStream: StreamData) {
                assert(false, "this should never happen")
            }
            
            func connection(_ connection: ServiceConnection, didReceiveMessage message: Message) {
                guard let payload = message.payload else { print("echo=<nil>"); return }
                let echo = String(data: payload, encoding: .utf8)
                print("echo=\(echo ?? "<nil>")")
            }
            
            func connection(didConnect connection: ServiceConnection) {
                print("TCP connection did cconnect")
            }
            
            func connection(didDisconnect connection: ServiceConnection, error: Error?) {
                print("TCP connection did disconnect")
            }
        }
        
        do {
            let serverAddress = IPAddress.localhost(withPort: serverPort)
            guard let connection = ServiceConnection(type: .tcp, remoteAddress: serverAddress) else { throw SocketError.internalError }
            connection.delegate = ConnectionDelegate()
            try connection.connect()
            print("TCP connection did connect")
            
            guard let writeData = testString.data(using: .utf8) else { throw SocketError.internalError }
            try connection.send(Message(type: 0, payload: writeData))
            print("TCP connection sent message")
        } catch (let error) {
            print("error=\(error)")
            throw error
        }
    }
    
    public func startUsingService(type: ConnectionType) throws {
        class ConnectionDelegate: ServiceConnectionDelegate {
            func connection(_ connection: ServiceConnection, didReceiveStream: StreamData) {
            }
            
            func connection(_ connection: ServiceConnection, didReceiveMessage message: Message) {
                guard let payload = message.payload else { print("echo=<nil>"); return }
                let echo = String(data: payload, encoding: .utf8)
                print("echo=\(echo ?? "<nil>")")
            }
            
            func connection(didConnect connection: ServiceConnection) {
                print("\(connection.type) connection did cconnect")
            }
            
            func connection(didDisconnect connection: ServiceConnection, error: Error?) {
                print("\(connection.type) connection did disconnect")
            }
        }
        
        do {
            let serverAddress = IPAddress.localhost(withPort: serverPort)
            guard let connection = ServiceConnection(type: type, remoteAddress: serverAddress) else { throw SocketError.internalError }
            self.connection = connection
            connection.delegate = ConnectionDelegate()
            try connection.connect()
            print("\(connection.type) connection did connect")
            
            guard let writeData = testString.data(using: .utf8) else { throw SocketError.internalError }
            try connection.send(Message(type: 0, payload: writeData))
            print("\(connection.type) connection sent message")
        } catch (let error) {
            print("error=\(error)")
            throw error
        }
    }
    
    public func startUsingTCP() throws {
        do {
            let serverAddress = IPAddress.localhost(withPort: serverPort)
            guard let clientSocket = TCPClientSocket(remoteAddress: serverAddress) else { throw SocketError.internalError }
            try clientSocket.connect()
            
            guard let writeData = testString.data(using: .utf8) else { throw SocketError.internalError }
            try clientSocket.write(writeData)
            guard let readData = try clientSocket.read(numberOfBytes: writeData.count) else { throw SocketError.internalError }
            let echo = String(data: readData, encoding: .utf8)
            print("echo=\(echo ?? "<nil>")")
        } catch (let error) {
            print("error=\(error)")
            throw error
        }
    }
    
    public func startUsingUDP() throws {
        do {
            let serverAddress = IPAddress.localhost(withPort: 30716)
            guard let clientSocket = UDPClientSocket(remoteAddress: serverAddress) else { throw SocketError.internalError }
            print("UDP socket: created ")
            guard let writeData = testString.data(using: .utf8) else { throw SocketError.internalError }
            do {
                try clientSocket.send(writeData)
                print("UDP socket: sent data")
            } catch (let sendError) {
                print("send.error=\(sendError)")
                throw sendError
            }
            
            guard let readData = try clientSocket.receive(numberOfBytes: writeData.count) else { return }
            print("UDP socket: received data")
            let echo = String(data: readData, encoding: .utf8)
            print("echo=\(echo ?? "<nil>")")
        } catch (let error) {
            print("error=\(error)")
            throw error
        }
    }
    
    public func sendPing(completion: ((Timestamp32) -> Void)?) throws {
        guard let connection = connection else { return }
        try connection.sendPing { completion?($0) }
    }
}
