//
//  ByteCodingExtensions.swift
//  manlobby
//
//  Created by Michael Sanford on 5/17/17.
//
//

import Foundation
import SwiftOnSockets

public extension ByteArchiver {
    public func encode(_ address: IPAddress) {
        encode(address.formatted)
    }
    
    public func encode(_ uuid: UUID) {
        encode(uuid.uuidString)
    }
}

public extension ByteUnarchiver {
    public func decodeIPAddress() throws -> IPAddress {
        let formatted = try decodeString()
        guard let address = IPAddress(formatted: formatted) else { throw ByteUnarchiverError.invalidInternalFormat }
        return address
    }
    
    public func decodeUUID() throws -> UUID {
        let uuidString = try decodeString()
        guard let uuid = UUID(uuidString: uuidString) else { throw ByteUnarchiverError.invalidInternalFormat }
        return uuid
    }
}
