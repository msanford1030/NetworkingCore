//
//  FixedArray.swift
//
//  Created by Michael Sanford on 9/22/16.
//  Copyright © 2017 flipside5. All rights reserved.
//

/// A `FixedArray` is an array like structure that may contain `nil` and, most importantly,
/// will always place an object at the lowest available index (i.e. a `nil` value). The index
/// is returned when adding an element to a `FixedArray`. For example, a `FixedArray` that is
/// represented as ["red", nil, "green"] may be accessed as `myArray[0]` and this will return
/// `"red"`. Similiarly, `myArray[1]` returns `nil`. When calling `myArray.add("yellow")`, the
/// index `1` is return, the first nil index, and the array now looks like ["red", "yellow", "green"].
/// A `FixedArray` has a fixed size (hence the name). therefore, in this example, calling
/// `myArray.add("blue") will fail and return `nil`.

import Foundation
import Dispatch

public protocol FixedArray {
    associatedtype ItemType

    var capacity: Int { get }
    var count: Int { get }
    var ordered: [ItemType] { get }
    var isEmpty: Bool { get }
    var isFull: Bool { get }
    
    subscript(index: Int) -> ItemType? { get set }
    
    mutating func add(_ item: ItemType) -> Int?
    @discardableResult mutating func remove(at index: Int) -> ItemType?
}

public struct FixedSafeArray<T>: FixedArray {
    private var array: FixedUnsafeArray<T>
    private let queue: DispatchQueue
    
    public init?(capacity: Int, isThreadSafe: Bool = false) {
        guard let array = FixedUnsafeArray<T>(capacity: capacity) else { return nil }
        self.array = array
        queue = DispatchQueue.init(label: "FixedArrayQueue")
    }
    
    public var capacity: Int {
        var value: Int?
        queue.sync {
            value = array.capacity
        }
        return value!
    }
    
    public var count: Int {
        var value: Int?
        queue.sync {
            value = array.count
        }
        return value!
    }
    
    public var ordered: [T] {
        var value: [T]?
        queue.sync {
            value = array.ordered
        }
        return value!
    }
    
    public var isEmpty: Bool {
        var value: Bool?
        queue.sync {
            value = array.isEmpty
        }
        return value!
    }
    
    public var isFull: Bool {
        var value: Bool?
        queue.sync {
            value = array.isFull
        }
        return value!
    }
    
    public subscript(index: Int) -> T? {
        get {
            var value: T?
            queue.sync {
                value = array[index]
            }
            return value
        }
        set {
            queue.sync {
                array[index] = newValue
            }
        }
    }
    
    public mutating func add(_ item: T) -> Int? {
        var value: Int?
        queue.sync {
            value = array.add(item)
        }
        return value
    }
    
    public mutating func remove(at index: Int) -> T? {
        var value: T?
        queue.sync {
            value = array.remove(at: index)
        }
        return value
    }
}

public struct FixedUnsafeArray<T>: FixedArray {
    private var state: FixedArrayState<T>

    public init?(capacity: Int) {
        guard (capacity >= 0) && (capacity < Int(UInt16.max)) else { return nil }
        state = FixedArrayState(capacity: capacity)
    }
    
    public var capacity: Int {
        return state.capacity
    }
    
    public var count: Int {
        return state.count
    }
    
    public var ordered: [T] {
        return state.ordered
    }
    
    public var isEmpty: Bool {
        return state.isEmpty
    }
    
    public var isFull: Bool {
        return state.isFull
    }
    
    public subscript(index: Int) -> T? {
        get {
            assert(state.isValid(index: index), "FixedArray: expected valid index; index=\(index)")
            guard state.isValid(index: index) else { return nil }
            return state.items[index]
        }
        set {
            assert(state.isValid(index: index), "FixedArray: expected valid index; index=\(index)")
            guard state.isValid(index: index) else { return }
            
            if let newNonNilValue = newValue {
                guard let newState = state.updated(at: index, item: newNonNilValue) else { return }
                state = newState
            } else {
                guard let (newState, _) = state.removed(at: index) else { return }
                state = newState
            }
        }
    }
    
    public mutating func add(_ item: T) -> Int? {
        guard let (newState, index) = state.added(item) else { return nil }
        state = newState
        return index
    }
    
    public mutating func remove(at index: Int) -> T? {
        guard let (newState, item) = state.removed(at: index) else { return nil }
        state = newState
        return item
    }
}

/*----------------------------------------------------------*/

private struct FixedArrayState<T> {
    let count: Int
    let capacity: Int
    let ordered: [T]
    let items: [T?]
    private let lowestNextIndex: Int
    private let highestUsedIndex: Int
    
    init(count: Int, items: [T?], lowestNextIndex: Int, highestUsedIndex: Int) {
        func ordered() -> [T] {
            guard count != 0 else { return [] }
            guard count == items.count else { return items.compactMap { $0 } }
            return items[0...highestUsedIndex].compactMap { $0 }
        }
        
        self.count = count
        self.items = items
        self.lowestNextIndex = lowestNextIndex
        self.highestUsedIndex = highestUsedIndex
        self.capacity = items.count
        self.ordered = ordered()
    }
    
    init(capacity: Int) {
        self.init(
            count: 0,
            items: Array<T?>(repeatElement(nil, count: capacity)),
            lowestNextIndex: 0,
            highestUsedIndex: -1)
    }
    
    var isEmpty: Bool {
        return count == 0
    }
    
    var isFull: Bool {
        return count >= capacity
    }
    
    func isValid(index: Int) -> Bool {
        return (index >= 0) && (index < capacity)
    }
    
    func updated(at index: Int, item: T) -> FixedArrayState<T>? {
        guard isValid(index: index) && (items[index] == nil) else { return nil }
        
        let newCount = count + 1
        var newItems = items
        newItems[index] = item
        
        let newLowestNextIndex: Int
        if newCount == newItems.count {
            newLowestNextIndex = -1
        } else {
            let updatedLowestNextIndex: Int? = newItems.enumerated().reduce(nil) { (result, info: (offset: Int, element: T?)) in
                guard result == nil else { return result }
                return info.element == nil ? info.offset : nil
            }
            guard let nextIndex = updatedLowestNextIndex else { return nil }
            newLowestNextIndex = nextIndex
        }
        
        let newHighestUsedIndex = max(index, highestUsedIndex)
        
        let newState = FixedArrayState(
            count: newCount,
            items: newItems,
            lowestNextIndex: newLowestNextIndex,
            highestUsedIndex: newHighestUsedIndex)
        return newState
        
    }
    
    func added(_ item: T) -> (FixedArrayState<T>, Int)? {
        guard count != capacity && lowestNextIndex >= 0 && items[lowestNextIndex] == nil else { return nil }
        
        let newCount = count + 1
        let resultIndex = lowestNextIndex
        var newItems: [T?] = []
        items.forEach { newItems.append($0) }
        newItems[resultIndex] = item
        let newHighestUsedIndex = max(resultIndex, highestUsedIndex)
        
        let newLowestNextIndex: Int
        if newCount == newItems.count {
            newLowestNextIndex = -1
            assert(newHighestUsedIndex == (newItems.count - 1), "FixedArray: internal error (highestUsedIndex is horked)")
        } else {
            var nextIndex = resultIndex + 1
            assert(isValid(index: nextIndex), "FixedArray: internal error (nextIndex is horked) - \(nextIndex)")
            while newItems[nextIndex] != nil {
                nextIndex += 1
                assert(isValid(index: nextIndex), "FixedArray: internal error (nextIndex is horked) - \(nextIndex)")
            }
            newLowestNextIndex = nextIndex
        }
        
        let newState = FixedArrayState(
            count: newCount,
            items: newItems,
            lowestNextIndex: newLowestNextIndex,
            highestUsedIndex: newHighestUsedIndex)
        
        return (newState, resultIndex)
    }
    
    func removed(at index: Int) -> (FixedArrayState<T>, T)? {
        guard isValid(index: index) && !isEmpty else { return nil }
        guard let item = items[index] else { return nil }
        
        let newCount = count - 1
        var newItems: [T?] = []
        items.forEach { newItems.append($0) }
        newItems[index] = nil
        
        let newLowestNextIndex: Int
        let newHighestUsedIndex: Int
        if newCount == 0 {
            newLowestNextIndex = 0
            newHighestUsedIndex = -1
        } else {
            newLowestNextIndex = lowestNextIndex < 0 ? index : min(index, lowestNextIndex)
            
            if index == highestUsedIndex {
                var previousIndex = highestUsedIndex - 1
                while newItems[previousIndex] == nil {
                    previousIndex -= 1
                    assert(isValid(index: previousIndex), "FixedArray: internal error (previousIndex is horked) - \(previousIndex)")
                }
                newHighestUsedIndex = previousIndex
            } else {
                newHighestUsedIndex = highestUsedIndex
            }
        }
        
        let newState = FixedArrayState(
            count: newCount,
            items: newItems,
            lowestNextIndex:
            newLowestNextIndex,
            highestUsedIndex: newHighestUsedIndex)
        
        return (newState, item)
    }
}
