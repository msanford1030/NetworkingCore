//
//  Session.swift
//
//  Created by Michael Sanford on 10/11/16.
//  Copyright © 2016 flipside5. All rights reserved.
//

import Foundation


protocol Session {
    func onDidConnectToUser(_ userID: UserID)
    func onDidDisconnectFromUser(_ userID: UserID)
    func disconnectFromUser(_ userID: UserID)
    func close()
}

protocol EventBasedSession: Session {
    func onDidReceive(message: Message, fromUser userID: UserID)
    func send(message: Message, toUser userID: UserID)
}

protocol RealTimeSession: Session {
    func onDidReceiveUserState(_ data: Data, fromUser userID: UserID)
    func onUpdateState(withTimestamp timestamp: Timestamp64)
    func currentState() -> Data
}
