//
//  SessionTypes.swift
//
//  Created by Michael Sanford on 10/11/16.
//  Copyright © 2017 flipside5. All rights reserved.
//

import Foundation

public typealias SessionID = Int
public typealias Watermark = Int
public typealias UserID = Int
