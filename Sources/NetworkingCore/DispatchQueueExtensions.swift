//
//  DispatchQueueExtensions.swift
//  NetworkingCore
//
//  Created by Michael Sanford on 7/8/17.
//
//

import Foundation
import Dispatch

fileprivate struct WorkItemRef {
    weak var item: DispatchWorkItem?
}

public extension DispatchQueue {
    func addOperation(_ delay: TimeInterval, handler: @escaping () -> ()) -> DispatchWorkItem {
        let item = DispatchWorkItem(block: handler)
        let when = DispatchTime.now() + delay
        asyncAfter(deadline: when, execute: item)
        return item
    }
    
    func addOperation(_ delay: TimeInterval, repeats: Bool, handler: @escaping () -> ()) -> DispatchWorkItem {
        guard repeats else { return addOperation(delay, handler: handler) }
        
        var internalItemRef: WorkItemRef = WorkItemRef(item: nil)
        let timerItem = DispatchWorkItem() { [weak self] in
            guard let item = internalItemRef.item, let strongSelf = self, !item.isCancelled else { return }
            let when = DispatchTime.now() + delay
            handler()
            strongSelf.asyncAfter(deadline: when, execute: item)
        }
        internalItemRef.item = timerItem
        let when = DispatchTime.now() + delay
        asyncAfter(deadline: when, execute: timerItem)
        return timerItem
    }
    
    func asyncAfter(timeInterval: TimeInterval, execute: @escaping (() -> Void)) {
        asyncAfter(deadline: .now() + Double(timeInterval), execute: execute)
    }
}

