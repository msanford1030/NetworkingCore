//
//  Message.swift
//
//  Created by Michael Sanford on 12/21/16.
//  Copyright © 2017 flipside5. All rights reserved.
//

import Foundation

public typealias MessageType = UInt8

public struct Message: ByteCoding {
    public let type: MessageType
    public let payload: Data?
    
    public init(type: MessageType, payload: Data? = nil) {
        self.type = type
        self.payload = payload
    }
    
    public func encode(with archiver: ByteArchiver) {
        archiver.encode(type)
        archiver.encode(payload)
    }
    
    public init?(unarchiver: ByteUnarchiver) {
        do {
            type = MessageType(try unarchiver.decodeUInt8())
            payload = try unarchiver.decodeData()
        } catch {
            return nil
        }
    }
}

public struct IdentifiableMessage: ByteCoding {
    public let identifier: UUID
    public let message: Message
    
    public init(identifier: UUID = UUID(), message: Message) {
        self.identifier = identifier
        self.message = message
    }
    
    public init(identifier: UUID = UUID(), type: MessageType, payload: Data?) {
        let message = Message(type: type, payload: payload)
        self.init(identifier: identifier, message: message)
    }
    
    public init(identifier: UUID = UUID(), type: MessageType, payloadInfo: ByteCoding) {
        self.init(identifier: identifier, type: type, payload: ByteArchiver.archive(for: payloadInfo))
    }
    
    public init?(unarchiver: ByteUnarchiver) {
        do {
            self.identifier = try unarchiver.decodeUUID()
            guard let message = Message.init(unarchiver: unarchiver) else { return nil }
            self.message = message
        } catch {
            return nil
        }
    }
    
    public func encode(with archiver: ByteArchiver) {
        archiver.encode(identifier)
        archiver.encode(message)
    }
}
