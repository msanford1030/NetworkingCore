//
//  ETPPacket.swift
//
//  Created by Michael Sanford on 12/20/16.
//  Copyright © 2017 flipside5. All rights reserved.
//

import Foundation

public enum ETPTransportValue {
    case keepAlive
    case ping(Timestamp32?)
    case pong(Timestamp32?)
    case message(Message)
}

/*------------------------------------------------*/

public struct ETPTransportHeader {
    
    public typealias Version = UInt8
    public typealias PayloadSize = UInt16
    public typealias PayloadType = UInt8

    public struct Layout {
        public static let size = MemoryLayout<Version>.size + MemoryLayout<PayloadType>.size + MemoryLayout<PayloadSize>.size
    }
    
    enum ReservedPayloadType: UInt8 {
        case keepAlive = 1
        case ping = 2
        case pong = 3
        
        var payloadType: PayloadType {
            return PayloadType(self.rawValue)
        }
    }

    public let version: Version
    public let payloadType: PayloadType
    public let payloadSize: PayloadSize
}

enum ETPArchiveError: Error {
    case internalError
}

enum ETPUnarchiveError: Error {
    case invalidHeader
    case invalidPayload
}

/*------------------------------------------------*/

public class ETPArchiver {
    
    private let clock: Clock?
    
    init(clock: Clock? = nil) {
        self.clock = clock
    }
    
    private func transportArchive(forType type: ETPTransportHeader.PayloadType, payload: Data? = nil) -> Data {
        
        func headerArchive(for header: ETPTransportHeader) -> Data {
            let archiver = ByteArchiver(capacity: ETPTransportHeader.Layout.size)
            archiver.encode(header.version)
            archiver.encode(header.payloadType)
            archiver.encode(header.payloadSize)
            
            let headerArchive = archiver.archive
            assert(headerArchive.count == ETPTransportHeader.Layout.size, "ETP header archive is horked")
            return headerArchive
        }
        
        guard let payload = payload else {
            let header = ETPTransportHeader(version: 1, payloadType: type, payloadSize: 0)
            return headerArchive(for: header)
        }
        let payloadSize = ETPTransportHeader.PayloadSize(payload.count)
        let header = ETPTransportHeader(version: 1, payloadType: type, payloadSize: payloadSize)
        return headerArchive(for: header) + payload
    }
    
    public var keepAliveArchive: Data {
        return transportArchive(forType: ETPTransportHeader.ReservedPayloadType.keepAlive.payloadType)
    }
    
    public var pingArchive: Data {
        guard let clock = clock else {
            return transportArchive(forType: ETPTransportHeader.ReservedPayloadType.ping.payloadType)
        }
        
        let archiver = ByteArchiver(capacity: MemoryLayout<Timestamp32>.size)
        archiver.encode(clock.now)
        return transportArchive(forType: ETPTransportHeader.ReservedPayloadType.ping.payloadType, payload: archiver.archive)
    }
    
    public func pongArchive(withTimestamp timestamp: Timestamp32? = nil) -> Data {
        guard let timestamp = timestamp else {
            return transportArchive(forType: ETPTransportHeader.ReservedPayloadType.pong.payloadType)
        }
        
        let archiver = ByteArchiver(capacity: MemoryLayout<Timestamp32>.size)
        archiver.encode(timestamp)
        return transportArchive(forType: ETPTransportHeader.ReservedPayloadType.pong.payloadType, payload: archiver.archive)
    }
    
    public func messageArchive(withMessage message: Message) -> Data {
        return transportArchive(forType: ETPTransportHeader.PayloadType(message.type), payload: message.payload)
    }
}

/*------------------------------------------------*/

public class ETPUnarchiver {
    
    private let clock: Clock?
    
    public init(clock: Clock? = nil) {
        self.clock = clock
    }
    
    public func unarchiveHeader(_ archive: Data) throws -> ETPTransportHeader {
        guard archive.count >= ETPTransportHeader.Layout.size else { throw ETPUnarchiveError.invalidHeader }
        let unarchiver = ByteUnarchiver(archive: archive)
        let version: ETPTransportHeader.Version = try unarchiver.decodeUInt8()
        let payloadType: ETPTransportHeader.PayloadType = try unarchiver.decodeUInt8()
        let payloadSize: ETPTransportHeader.PayloadSize = try unarchiver.decodeUInt16()
        return ETPTransportHeader(version: version, payloadType: payloadType, payloadSize: payloadSize)
    }

    public func unarchiveValue(header: ETPTransportHeader, payload: Data? = nil) throws -> ETPTransportValue {
        switch header.payloadType {
        case ETPTransportHeader.ReservedPayloadType.keepAlive.rawValue:
            return .keepAlive
            
        case ETPTransportHeader.ReservedPayloadType.ping.rawValue:
            guard let payload = payload, payload.count == MemoryLayout<Timestamp32>.size else { return .ping(nil) }
            let unarchiver = ByteUnarchiver(archive: payload)
            let timestamp: Timestamp32 = try unarchiver.decodeUInt32()
            return .ping(timestamp)
            
        case ETPTransportHeader.ReservedPayloadType.pong.rawValue:
            guard let clock = clock, let payload = payload, payload.count == MemoryLayout<Timestamp32>.size else { return .pong(nil) }
            let unarchiver = ByteUnarchiver(archive: payload)
            let timestamp: Timestamp32 = try unarchiver.decodeUInt32()
            return .pong(clock.now - timestamp)
            
        default:
            guard let payload = payload, payload.count > 0 else { return .message(Message(type: header.payloadType)) }
            guard payload.count == Int(header.payloadSize) else { throw ETPUnarchiveError.invalidPayload }
            return .message(Message(type: header.payloadType, payload: payload))
        }
    }
}

/*------------------------------------------------*/

public class ETPSession {
    let clock = Clock(startImmediately: true)
    public let archiver: ETPArchiver
    public let unarchiver: ETPUnarchiver
    
    public init() {
        archiver = ETPArchiver(clock: clock)
        unarchiver = ETPUnarchiver(clock: clock)
    }
}
