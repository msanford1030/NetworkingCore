//
//  ByteCoding.swift
//
//  Created by Michael Sanford on 1/20/17.
//  Copyright © 2017 flipside5. All rights reserved.
//

import Foundation

public protocol ByteCoding {
    func encode(with archiver: ByteArchiver)
    init?(unarchiver: ByteUnarchiver)
}
