//
//  Stack.swift
//
//  Created by Michael Sanford on 9/14/16.
//  Copyright © 2017 flipside5. All rights reserved.
//

import Foundation

public struct Stack<T>: MutableSequence {
    public struct StackIterator: IteratorProtocol {
        private var currentNode: Node?
        
        fileprivate init(firstNode: Node?) {
            currentNode = firstNode
        }
        
        public mutating func next() -> T? {
            guard let currentNode = self.currentNode else { return nil }
            self.currentNode = currentNode.next
            return currentNode.value
        }
    }
    
    public class Node {
        fileprivate let value: T
        fileprivate let next: Node?
        
        fileprivate init(value: T, next: Node? = nil) {
            self.value = value
            self.next = next
        }
    }
    
    public private(set) var count: Int = 0
    private var topNode: Node? = nil
    
    public init() {}
    
    public mutating func add(_ item: T) {
        let node = Node(value: item, next: topNode)
        topNode = node
        count += 1
    }
    
    @discardableResult public mutating func remove() -> T? {
        guard let topNode = self.topNode else { return nil }
        count -= 1
        self.topNode = topNode.next
        return topNode.value
    }
    
    public var first: T? {
        return topNode?.value
    }
    
    public var isEmpty: Bool {
        return topNode == nil
    }
    
    /// MARK: Sequence
    public func makeIterator() -> StackIterator {
        return StackIterator(firstNode: topNode)
    }
    
    public var underestimatedCount: Int {
        return count
    }
    
    /// MARK: Stack-like API
    public mutating func push(_ item: T) {
        add(item)
    }
    
    @discardableResult public mutating func pop() -> T? {
        return remove()
    }
}
